;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

;; Following features were referenced:
;; allegro
;; bsd
;; ccl
;; clisp
;; clx
;; cmu
;; darwin
;; echo-area-is-separate-window
;; excl
;; fixme
;; gbnil
;; internal-caps-visible
;; linux
;; nil
;; nilamb
;; nilamb-duplicate
;; nilgb
;; openmcl
;; or
;; port-user-lisp-lib
;; port-user-lispeval
;; port-user-mh
;; port-user-netnews
;; port-user-unixcoms
;; sbcl
;; scl
;; x86



(defparameter *hemlock-base-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (parse-namestring *load-truename*)))

(asdf:defsystem :xomax
     :pathname #.(make-pathname
                        :directory
                        (pathname-directory *hemlock-base-directory*)
                        :defaults *hemlock-base-directory*)
     :around-compile "hemlock-internals::build-documentation"
     :depends-on (:xomax.base :clx :chanl)
    :components
    ((:module clx-1
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *hemlock-base-directory*)
              :components
              ((:file "xdk-window")
               (:file "hunk-draw" :depends-on ("xdk-window"))
               (:file "ioconnections")
               (:file "bitmap-input")
               (:file "bit-display" :depends-on ("hunk-draw"))
               (:file xdk-display)
               (:file "bit-screen")
               (:file "bitmap")
               (:file util)))))
