#!/bin/bash

texi2pdf xomax.texinfo

# Delete everything but the pdf
rm -f *aux
rm -f *cp
rm -f *cps
rm -f *fn
rm -f *ky
rm -f *log
rm -f *pg
rm -f *t2d
rm -f *toc
rm -f *tp
rm -f *vr
rm -f *vrs
