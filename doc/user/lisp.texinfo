@comment{-*- Dictionary: /afs/cs/project/clisp/scribe/hem/hem; Mode: spell; Package: Hemlock -*-}
@node  Interacting With Lisp
@chapter Interacting With Lisp
@cindex lisp, interaction with 

Lisp encourages highly interactive programming environments by requiring
decisions about object type and function definition to be postponed until run
time.  Xomax supports interactive programming in Lisp by providing
incremental redefinition and environment examination commands.  Xomax also
uses Unix TCP sockets to support multiple Lisp processes, each of which may be
on any machine.


@section Eval Servers
@cindex eval-servers 
@cindex eval servers 

Xomax runs in the editor process and interacts with other Lisp processes
called @i[eval servers].  A user's Lisp program normally runs in an eval
server process.  The separation between editor and eval server has several
advantages:

@itemize

@item
The editor is protected from any bad things which may happen while debugging a
Lisp program.

@item
Editing may occur while running a Lisp program.

@item
The eval server may be on a different machine, removing the load from the
editing machine.

@item
Multiple eval servers allow the use of several distinct Lisp environments.
@end itemize

Instead of providing an interface to a single Lisp environment, Xomax
coordinates multiple Lisp environments.


@subsection The Current Eval Server
@cindex current eval server 
Although Xomax can be connected to several eval servers simultaneously, one
eval server is designated as the @i[current eval server].  This is the eval
server used to handle evaluation and compilation requests.  Eval servers are
referred to by name so that there is a convenient way to discriminate between
servers when the editor is connected to more than one.  The current eval server
is normally globally specified, but it may also be shadowed locally in specific
buffers.

@deffn Command "Set Eval Server"
@end deffn

@deffn Command "Set Buffer Eval Server"]
@end deffn

@deffn Command "Current Eval Server"]
@code{Set Eval Server} prompts for the name of an eval server and makes it the
the current eval server.  @code{Set Buffer Eval Server} is the same except that
is sets the eval server for the current buffer only.  @code{Current Eval Server}
displays the name of the current eval server in the echo area, taking any
buffer eval server into consideration.  See also @code{Set Compile Server}.
@end deffn


@subsection Slaves
@cindex slave buffers 
@cindex slaves 
For now, all eval servers are @i[slaves].  A slave is a Lisp process that uses
a typescript (see page @xref{typescripts}) to run its top-level
@code{read-eval-print} loop in a Xomax buffer.  We refer to the buffer that a
slave uses for I/O as its @i[interactive] or @i[slave] buffer.  The name of the
interactive buffer is the same as the eval server's name.

@cindex background buffers 
Xomax creates a @i[background] buffer for each eval server.  The background
buffer's name is @w<@code{Background }@i{name}>, where @i[name] is the name of
the eval server.  Slaves direct compiler warning output to the background
buffer to avoid cluttering up the interactive buffer.

Xomax locally sets @code{Current Eval Server} in interactive and background
buffers to their associated slave.  When in a slave or background buffer, eval
server requests will go to the associated slave, regardless of the global value
of @code{Current Eval Server}.

@deffn Command "Select Slave", bind (C-M-c)
This command changes the current buffer to the current eval server's
interactive buffer.  If the current eval server is not a slave, then it beeps.
If there is no current eval server, then this creates a slave (see section
@ref[slave-creation]).  If a prefix argument is supplied, then this creates a
new slave regardless of whether there is a current eval server.  This command
is the standard way to create a slave.

The slave buffer is a typescript (see page @xref{typescripts}) the slave
uses for its top-level @code{read-eval-print} loop.
@end deffn

@deffn Command "Select Background", bind (C-M-C)
This command changes the current buffer to the current eval server's background
buffer.  If there is no current eval server, then it beeps.
@end deffn


@subsection Slave Creation and Destruction
@cindex slave-creation 
When Xomax first starts up, there is no current eval server.  If there is no
a current eval server, commands that need to use the current eval server will
create a slave as the current eval server.

If an eval server's Lisp process terminates, then we say the eval server is
dead.  Xomax displays a message in the echo area, interactive, and
background buffers whenever an eval server dies.  If the user deletes an
interactive or background buffer, the associated eval server effectively
becomes impotent, but Xomax does not try to kill the process.  If a command
attempts to use a dead eval server, then the command will beep and display a
message.

@defvar "Confirm Slave Creation", val {t}
If this variable is true, then Xomax always prompts the user for
confirmation before creating a slave.
@end defvar

@defvar "Ask About Old Servers", val {t}
If this variable is true, and some slave already exists, Xomax prompts the
user for the name of an existing server when there is no current server,
instead of creating a new one.
@enddefhvar

@deffn Command "Editor Server Name"
This command echos the editor server's name, the machine and port of the
editor, which is suitable for use with the Lisp processes -slave switch.
See section @ref[slave-switch].
@end deffn

@deffn Command "Accept Slave Connections"
This command cause Xomax to accept slave connections, and it displays the
editor server's name, which is suitable for use with the Lisp processes -slave
switch.  See section @ref[slave-switch].  Supplying an argument causes this
command to inhibit slave connections.
@end deffn

@defvar "Slave Utility", val {"/usr/misc/.lisp/bin/lisp"}
@end defvar

@defvar  "Slave Utility Switches"
A slave is started by running the program @code{Slave Utility Name} with
arguments specified by the list of strings @code{Slave Utility Switches}.  This
is useful primarily when running customized Lisp systems.  For example,
setting @code{Slave Utility Switches} to @code{("-core" "my.core")} will cause
"@code{/usr/hqb/my.core}" to be used instead of the default core image.

The @code{-slave} switch and the editor name are always supplied as arguments, and
should remain unspecified in @code{Slave Utility Switches}.
@end defvar

@deffn Command "Kill Slave"
@end deffn

@deffn Command "Kill Slave and Buffers"]
@code{Kill Slave} prompts for a slave name, aborts any operations in the slave,
tells the slave to @code{quit}, and shuts down the connection to the specified
eval server.  This makes no attempt to assure the eval server actually dies.

@code{Kill Slave and Buffers} is the same as @code{Kill Slave}, but it also
deletes the interactive and background buffers.
@end deffn


@subsection Eval Server Operations

@cindex operations 
@cindex eval server operations 
@cindex operations, eval server 
Xomax handles requests for compilation or evaluation by queuing an
@i[operation] on the current eval server.  Any number of operations may be
queued, but each eval server can only service one operation at a time.
Information about the progress of operations is displayed in the echo area.

@deffn Command "Abort Operations", bind (C-c a)
This command aborts all operations on the current eval server, either queued or
in progress.  Any operations already in the @code{Aborted} state will be flushed.
@end deffn

@deffn Command "List Operations", bind (C-c l)
This command lists all operations which have not yet completed.  Along with a
description of the operation, the state and eval server is displayed.  The
following states are used:

@table @dfn
@item @code{Unsent}
The operation is in local queue in the editor, and hasn't been sent
yet.

@item @code{Pending}
The operation has been sent, but has not yet started execution.

@item @code{Running}
The operation is currently being processed.

@item @code{Aborted}
The operation has been aborted, but the eval server has not yet
indicated termination.

@end table

@end deffn


@section Typescripts
@cindex typescripts 
@cindex typescripts 

Both slave buffers and background buffers are typescripts.  The typescript
protocol allows other processes to do stream-oriented interaction in a Xomax
buffer similar to that of a terminal.  When there is a typescript in a buffer,
the @code{Typescript} minor mode is present.  Some of the commands described in
this section are also used by @code{Eval} mode (page @xref{eval-mode}.)

Typescripts are simple to use.  Xomax inserts output from the process into
the buffer.  To give the process input, use normal editing to insert the input
at the end of the buffer, and then type @strong[Return to confirm sending the
input to the process.

@deffn Command "Confirm Typescript Input", 
        stuff (bound to @strong{Return] in @code{Typescript} mode)}
@end deffn

@defvar "Unwedge Interactive Input Confirm", val {t}
This command sends text that has been inserted at the end of the current buffer
to the process reading on the buffer's typescript.  Before sending the text,
Xomax moves the point to the end of the buffer and inserts a newline.

Input may be edited as much as is desired before it is confirmed; the result
of editing input after it has been confirmed is unpredictable.  For this reason,
it is desirable to postpone confirming of input until it is actually complete.
The @code{Indent New Line} command is often useful for inserting newlines
without confirming the input.

If the process reading on the buffer's typescript is not waiting for input,
then the text is queued instead of being sent immediately.  Any number of
inputs may be typed ahead in this fashion.  Xomax makes sure that the inputs
and outputs get interleaved correctly so that when all input has been read, the
buffer looks the same as it would have if the input had not been typed ahead.

If the buffer's point is before the start of the input area, then various
actions can occur.  When set, @code{Unwedge Interactive Input Confirm} causes
Xomax to ask the user if it should fix the input buffer which typically
results in ignoring any current input and refreshing the input area at the end
of the buffer.  This also has the effect of throwing the slave Lisp to top
level, which aborts any pending operations or queued input.  This is the only
way to be sure the user is cleanly set up again after messing up the input
region.  When this is @code{nil}, Xomax simply beeps and tells the user in the
@code{Echo Area} that the input area is invalid.
@end defvar

@deffn Command "Kill Interactive Input", 
    stuff (bound to @strong{M-i} in @code{Typescript} and @code{Eval} modes)]
This command kills any input that would have been confirmed by @strong[Return.
@end deffn

@deffn Command "Next Interactive Input",  
        stuff (bound to @strong{M-n] in @code{Typescript} and @code{Eval} modes)}
        @end deffn
        
@deffn Command "Previous Interactive Input",
        stuff (bound to @strong{M-p} in @code{Typescript} and @code{Eval} modes)]
        @end deffn
        
@deffn Command "Search Previous Interactive Input",
	stuff (bound to @strong{M-P} in @code{Typescript} and @code{Eval} modes)]
@end deffn

@defvar "Interactive History Length", val {10}
@end defvar

@defvar "Minimum Interactive Input Length", val {2}

@cindex history, typescript 
Xomax maintains a history of interactive inputs.  @code{Next Interactive
Input} and @code{Previous Interactive Input} step forward and backward in the
history, inserting the current entry in the buffer.  The prefix argument is
used as a repeat count.

@code{Search Previous Interactive Input} searches backward through the
interactive history using the current input as a search string.  Consecutive
invocations repeat the previous search.

@code{Interactive History Length} determines the number of entries with which
Xomax creates the buffer-specific histories.  Xomax only adds an input
region to the history if its number of characters exceeds @code{Minimum
Interactive Input Length}.
@end defvar

@deffn Command "Reenter Interactive Input",
	stuff (bound to @strong{C-Return} in @code{Typescript} and @code{Eval} modes)]
 This copies to the end of the buffer the form to the left of the buffer's
point.  When the current region is active, this copies it instead.  This is
sometimes easier to use to get a previous input that is either so far back that
it has fallen off the history or is visible and more readily @i[yanked than
gotten with successive invocations of the history commands.
@end deffn

@deffn Command "Interactive Beginning of Line", 
        stuff (bound to @strong{C-a} in @code{Typescript} and @code{Eval} modes)]
This command is identical to @code{Beginning of Line} unless there is no
prefix argument and the point is on the same line as the start of the current
input; then it moves to the beginning of the input.  This is useful since it
skips over any prompt which may be present.
@end deffn

@defvar "Input Wait Alarm", val {:loud-message}
@end defvar

@defvar "Slave GC Alarm", val {:message}
@code{Input Wait Alarm} determines what action to take when a slave Lisp goes
into an input wait on a typescript that isn't currently displayed in any
window.  @code{Slave GC Alarm} determines what action to take when a slave
notifies that it is GC'ing.

The following are legal values:

@table @dfn

@item @key{loud-message}
Beep and display a message in the echo area indicating
which buffer is waiting for input.

@item @key{message}
Display a message, but don't beep.

@item @code{nil}
Don't do anything.

@end table

@end defvar

@deffn Command "Typescript Slave BREAK", bind (Typescript: H-b)
@end deffn

@deffn Command "Typescript Slave to Top Level", bind (Typescript: H-g)]
@end deffn

@deffn Command "Typescript Slave Status", bind (Typescript: H-s)]
Some typescripts have associated information which these commands access
allowing Xomax to control the process which uses the typescript.

@code{Typescript Slave BREAK} puts the current process in a break loop so that
you can be debug it.  This is similar in effect to an interrupt signal (@code{^C}
or @code{^\} in the editor process).

@code{Typescript Slave to Top Level} causes the current process to throw to the
top-level @code{read-eval-print} loop.  This is similar in effect to a quit signal
(@code{^\}).

@code{Typescript Slave Status} causes the current process to print status
information on @var[error-output]:
@example
; Used 0:06:03, 3851 faults.  In: SYSTEM:SERVE-EVENT
@end example
The message displays the process run-time, the total number of page faults and
the name of the currently running function.   This command is useful for
determining whether the slave is in an infinite loop, waiting for input, or
whatever.
@end deffn


@section The Current Package
@cindex lisp-package 
@cindex package 
The current package is the package which Lisp interaction commands use.  The
current package is specified on a per-buffer basis, and defaults to "@code{USER}".
If the current package does not exist in the eval server, then it is created.
If evaluation is being done in the editor process and the current package
doesn't exist, then the value of @code{*package*} is used.  The current package is
displayed in the modeline (see section @ref[modelines].)  Normally the package
for each file is specified using the @code{Package} file option (see page
@xref{file-options}.)

When in a slave buffer, the current package is controlled by the value of
@var[package] in that Lisp process.  Modeline display of the current package
is inhibited in this case.

@deffn Command "Set Buffer Package"
This command prompts for the name of a package to make the local package in the
current buffer.  If the current buffer is a slave, background, or eval buffer,
then this sets the current package in the associated eval server or editor
Lisp.  When in an interactive buffer, do not use @code{in-package}; use this
command instead.
@end deffn


@section Compiling and Evaluating Lisp Code

@cindex compilation 
@cindex evaluation 
These commands can greatly speed up
the edit/debug cycle since they enable incremental reevaluation or
recompilation of changed code, avoiding the need to compile and load an
entire file.  

@deffn Command "Evaluate Expression", bind (M-Escape)
This command prompts for an expression and prints the result of its evaluation
in the echo area.  If an error happens during evaluation, the evaluation is
simply aborted, instead of going into the debugger.  This command doesn't
return until the evaluation is complete.
@end deffn

@deffn Command "Evaluate Defun", bind (C-x C-e)
@end deffn

@deffn Command "Evaluate Region"]
@end deffn

@deffn Command "Evaluate Buffer"]
These commands evaluate text out of the current buffer, reading the current
defun, the region and the entire buffer, respectively.  The result of the
evaluation of each form is displayed in the echo area.  If the region is
active, then @code{Evaluate Defun} evaluates the current region, just like 
@code{Evaluate Region}.
@end deffn

@deffn Command "Macroexpand Expression", bind (C-M)
This command shows the macroexpansion of the next expression in the null
environment in a pop-up window.  With an argument, it uses @code{macroexpand}
instead of @code{macroexpand-1}.
@end deffn

@deffn Command "Re-evaluate Defvar"
This command is similar to @code{Evaluate Defun}.  It is used for force the
re-evaluation of a @code{defvar} init form.  If the current top-level form is a
@code{defvar}, then it does a @code{makunbound} on the variable, and evaluates the
form.
@end deffn

@deffn Command "Compile Defun", bind (C-x C-c)
@end deffn

@deffn Command "Compile Region"]
These commands compile the text in the current defun and the region,
respectively.  If the region is active, then @code{Compile Defun} compiles the
current region, just like @code{Compile Region}.
@end deffn

@deffn Command "Load File"
@end deffn

@defvar "Load Pathname Defaults", val {nil}
This command prompts for a file and loads it into the current eval server using
@code{load}.  @code{Load Pathname Defaults} contains the default pathname for this
command.  This variable is set to the file loaded; if it is @code{nil}, then there is
no default.  This command also uses the @code{Remote Compile File} variable.
@end defvar

@section Compiling Files
These commands are used to compile source ("@code{.lisp}") files, producing binary
("@code{.fasl}") output files.  Note that unlike the other compiling and evalating
commands, this does not have the effect of placing the definitions in the
environment; to do so, the binary file must be loaded.

@deffn Command "Compile Buffer File", bind (C-x c)
@end deffn

@defvar "Compile Buffer File Confirm", val {t}
This command asks for confirmation, then saves the current buffer (when
modified) and compiles the associated file.  The confirmation prompt indicates
intent to save and compile or just compile.  If the buffer wasn't modified, and
a comparison of the write dates for the source and corresponding binary
("@code{.fasl}") file suggests that recompilation is unnecessary, the confirmation
also indicates this.  A prefix argument overrides this test and forces
recompilation.  Since there is a complete log of output in the background
buffer, the creation of the normal error output ("@code{.err}") file is inhibited.

Setting @code{Compile Buffer File Confirm} to @code{nil} inhibits confirmation, except
when the binary is up to date and a prefix argument is not supplied.
@end defvar

@deffn Command "Compile File"
This command prompts for a file and compiles that file, providing a convenient
way to compile a file that isn't in any buffer.  Unlike 
@code{Compile Buffer File}, this command doesn't do any consistency checks such
as checking whether the source is in a modified buffer or the binary is up to
date.
@end deffn

@deffn Command "Compile Group"
@end deffn

@deffn Command "List Compile Group"]
@cindex compile-group-command 
@cindex group, compilation 

@code{Compile Group} does a @code{Save All Files} and then compiles
every "@code{.lisp}" file for which the corresponding "@code{.fasl}" file is
older or nonexistent.  The files are compiled in the order in which
they appear in the group definition.  A prefix argument forces
compilation of all "@code{.lisp}" files.

@code{List Compile Group} lists any files that would be compiled by
@code{Compile Group}.  All Modified files are saved before checking to generate
a consistent list.
@end deffn 

@deffn Command "Set Compile Server"
@end deffn

@deffn Command "Set Buffer Compile Server"]
@end deffn

@deffn Command "Current Compile Server"]
These commands are analogous to @code{Set Eval Server}, @code{Set Buffer Eval
Server} and @code{Current Eval Server}, but they determine the eval server used
for file compilation requests.  If the user specifies a compile server, then
the file compilation commands send compilation requests to that server instead
of the current eval server.

Having a separate compile server makes it easy to do compilations in the
background while continuing to interact with your eval server and editor.  The
compile server can also run on a remote machine relieving your active
development machine of the compilation effort.
@end deffn

@deffn Command "Next Compiler Error", bind (H-n)
@end deffn

@deffn Command "Previous Compiler Error", bind (H-p)]
These commands provides a convenient way to inspect compiler errors.  First it
splits the current window if there is only one window present.  Xomax
positions the current point in the first window at the erroneous source code
for the next (or previous) error.  Then in the second window, it displays the
error beginning at the top of the window.  Given an argument, this command
skips that many errors.
@end deffn

@deffn Command "Flush Compiler Error Information"
This command relieves the current eval server of all infomation about errors
encountered while compiling.  This is convenient if you have been compiling a
lot, but you were ignoring errors and warnings.  You don't want to step through
all the old errors, so you can use this command immediately before compiling a
file whose errors you intend to edit.
@end deffn


@defvar "Remote Compile File", val {nil}
When true, this variable causes file compilations to be done using the RFS
remote file system mechanism by prepending "@code{/../}@i[host]" to the file being
compiled.  This allows the compile server to be run on a different machine, but
requires that the source be world readable.  If false, commands use source
filenames directly.  Do NOT use this to compile files in AFS.
@end defvar


@section Querying the Environment
@cindex documentation, lisp 
These commands are useful for obtaining various random information from the
Lisp environment.

@deffn Command "Describe Function Call", bind (C-M-A)
@end deffn

@deffn Command "Describe Symbol", bind (C-M-S)]
@code{Describe Function Call} uses the current eval server to describe the
symbol found at the head of the currently enclosing list, displaying the output
in a pop-up window.  @code{Describe Symbol} is the same except that it describes
the symbol at or before the point.  These commands are primarily useful for
finding the documentation for functions and variables.  If there is no
currently valid eval server, then this command uses the editor Lisp's
environment instead of trying to spawn a slave.
@end deffn


@section Editing Definitions
The Lisp compiler annotates each compiled function object with the source
file that the function was originally defined from.  The definition editing
commands use this information to locate and edit the source for functions
defined in the environment.

@deffn Command "Edit Definition"
@end deffn

@deffn Command "Goto Definition", bind (C-M-F)]
@end deffn

@deffn Command "Edit Command Definition"]
@code{Edit Definition} prompts for the name of a function, and then uses the
current eval server to find out in which file the function is defined.  If
something other than @code{defun} or @code{defmacro} defined the function, then this
simply reads in the file, without trying to find its definition point within
the file.  If the function is uncompiled, then this looks for it in the current
buffer.  If there is no currently valid eval server, then this command uses the
editor Lisp's environment instead of trying to spawn a slave.

@code{Goto Definition} edits the definition of the symbol at the beginning of
the current list.

@code{Edit Command Definition} edits the definition of a Xomax command.  By
default, this command does a keyword prompt for the command name (as in an
extended command).  If a prefix argument is specified, then instead prompt for
a key and edit the definition of the command bound to that key.
@end deffn

@deffn Command "Add Definition Directory Translation"
@end deffn

@deffn Command "Delete Definition Directory Translation"]
The defining file is recorded as an absolute pathname.  The definition editing
commands have a directory translation mechanism that allow the sources to be
found when they are not in the location where compilation was originally done.
@code{Add Definition Directory Translation} prompts for two directory
namestrings and causes the first to be mapped to the second.  Longer (more
specific) directory specifications are matched before shorter (more general)
ones.

@code{Delete Definition Directory Translation} prompts for a directory
namestring and deletes it from the directory translation table.
@end deffn

@defvar "Editor Definition Info", val {nil}
When this variable is true, the editor Lisp is used to determine definition
editing information, otherwise the current eval server is used.  This variable
is true in @code{Eval} and @code{Editor} modes.
@end defvar


@section Debugging
These commands manipulate the slave when it is in the debugger and provide
source editing based on the debugger's current frame.  These all affect the
@code{Current Eval Server}.


@subsection Changing Frames

@deffn Command "Debug Down", bind (C-M-H-d)
This command moves down one debugger frame.
@end deffn

@deffn Command "Debug Up", bind (C-M-H-u)
This command moves up one debugger frame.
@end deffn

@deffn Command "Debug Top", bind (C-M-H-t)
This command moves to the top of the debugging stack.
@end deffn

@deffn Command "Debug Bottom", bind (C-M-H-b)
This command moves to the bottom of the debugging stack.
@end deffn

@deffn Command "Debug Frame", bind (C-M-H-f)
This command moves to the absolute debugger frame number indicated by the
prefix argument.
@end deffn


@subsection Getting out of the Debugger

@deffn Command "Debug Quit", bind (C-M-H-q)
This command throws to top level out of the debugger in the @code{Current Eval
Server}.
@end deffn

@deffn Command "Debug Go", bind (C-M-H-g)
This command tries the @code{continue} restart in the @code{Current Eval Server}.
@end deffn

@deffn Command "Debug Abort", bind (C-M-H-a)
This command executes the ABORT restart in the @code{Current Eval Server}.
@end deffn

@deffn Command "Debug Restart", bind (C-M-H-r)
This command executes the restart indicated by the prefix argument in the
@code{Current Eval Server}.  The debugger enumerates the restart cases upon
entering it.
@end deffn


@subsection Getting Information

@deffn Command "Debug Help", bind (C-M-H-h)
This command in prints the debugger's help text.
@end deffn

@deffn Command "Debug Error", bind (C-M-H-e)
This command prints the error condition and restart cases displayed upon
entering the debugger.
@end deffn

@deffn Command "Debug Backtrace", bind (C-M-H-B)
This command executes the debugger's @code{backtrace} command.
@end deffn

@deffn Command "Debug Print", bind (C-M-H-p)
This command prints the debugger's current frame in the same fashion as the
frame motion commands.
@end deffn

@deffn Command "Debug Verbose Print", bind (C-M-H-P)
This command prints the debugger's current frame without elipsis.
@end deffn

@deffn Command "Debug Source", bind (C-M-H-s)
This command prints the source form for the debugger's current frame.
@end deffn

@deffn Command "Debug Verbose Source"
This command prints the source form for the debugger's current frame with
surrounding forms for context.
@end deffn

@deffn Command "Debug List Locals", bind (C-M-H-l)
This prints the local variables for the debugger's current frame.
@end deffn


@subsection Editing Sources

@deffn Command "Debug Edit Source", bind (C-M-H-S)
This command attempts to place you at the source location of the debugger's
current frame.  Not all debugger frames represent function's that were compiled
with the appropriate debug-info policy.  This beeps with a message if it is
unsuccessful.
@end deffn


@subsection Miscellaneous

@deffn Command "Debug Flush Errors", bind (C-M-H-F)
This command toggles whether the debugger ignores errors or recursively enters
itself.
@end deffn




@section Manipulating the Editor Process
When developing Xomax customizations, it is useful to be able to manipulate
the editor Lisp environment from Xomax.

@deffn Command "Editor Describe", bind (Home t, C-_ t)
This command prompts for an expression, and then evaluates and describes it
in the editor process.
@end deffn

@deffn Command "Room"
Call the @code{room} function in the editor process, displaying information
about allocated storage in a pop-up window.
@end deffn

@deffn Command "Editor Load File"
This command is analogous to @code{Load File}, but loads the file into the
editor process.
@end deffn


@subsection Editor Mode
When @code{Editor} mode is on, alternate versions of the Lisp interaction
commands are bound in place of the eval server based commands.  These commands
manipulate the editor process instead of the current eval server.  Turning on
editor mode in a buffer allows incremental development of code within the
running editor.

@deffn Command "Editor Mode"
This command turns on @code{Editor} minor mode in the current buffer.  If it is
already on, it is turned off.  @code{Editor} mode may also be turned on using
the @code{Mode} file option (see page @xref{file-options}.)
@end deffn

@deffn Command "Editor Compile Defun",
	stuff (bound to @strong{C-x C-c} in @code{Editor} mode)
        @end deffn
        
@deffn Command "Editor Compile Region"]
@end deffn

@deffn Command "Editor Evaluate Buffer"]
@end deffn

@deffn Command "Editor Evaluate Defun",
	stuff (bound to @strong{C-x C-e} in @code{Editor} mode)]
        @end deffn
        
@deffn Command "Editor Evaluate Region"]
@end deffn

@deffn Command "Editor Macroexpand Expression", bind (Editor: C-M)]
@end deffn

@deffn Command "Editor Re-evaluate Defvar"]
@end deffn

@deffn Command "Editor Describe Function Call",
	stuff (bound to @strong{C-M-A} in @code{Editor} mode)]
        @end deffn
        
@deffn Command "Editor Describe Symbol",
	stuff (bound to @strong{C-M-S} in @code{Editor} mode)]
These commands are similar to the standard commands, but modify or examine the
Lisp process that Xomax is running in.  Terminal I/O is done on the
initial window for the editor's Lisp process.  Output is directed to a pop-up
window or the editor's window instead of to the background buffer.
@end deffn

@deffn Command "Editor Compile Buffer File"
@end deffn

@deffn Command "Editor Compile File"]
@end deffn

@deffn Command "Editor Compile Group"
In addition to compiling in the editor process, these commands differ from the
eval server versions in that they direct output to the the 
@code{Compiler Warnings} buffer.
@end deffn

@deffn Command "Editor Evaluate Expression",
     stuff (bound to @strong{M-Escape} in @code{Editor} mode and @strong{C-M-Escape})] 
This command prompts for an expression and evaluates it in the editor process.
The results of the evaluation are displayed in the echo area.
@end deffn


@subsection Eval Mode
@cindex eval-mode 
@cindex modes, eval 
@code{Eval} mode is a minor mode that simulates a @code{read}
@code{eval} @code{print} loop running within the editor process.  Since Lisp
program development is usually done in a separate eval server process (see page
@xref{eval-servers}), @code{Eval} mode is used primarily for debugging code
that must run in the editor process.  @code{Eval} mode shares some commands with
@code{Typescript} mode: see section @ref[typescripts].

@code{Eval} mode doesn't completely support terminal I/O: it binds
@var[standard-output] to a stream that inserts into the buffer and
@var[standard-input to a stream that signals an error for all operations.
Xomax cannot correctly support the interactive evaluation of forms that read
from the @code{Eval} interactive buffer.

@deffn Command "Select Eval Buffer"
This command changes to the @code{Eval} buffer, creating one if it doesn't
already exist.  The @code{Eval} buffer is created with @code{Lisp} as the major
mode and @code{Eval} and @code{Editor} as minor modes. 
@end deffn

@deffn Command "Confirm Eval Input",
        stuff (bound to @strong{Return} in @code{Eval} mode)
This command evaluates all the forms between the end of the last output and
the end of the buffer, inserting the results of their evaluation in the buffer.
This beeps if the form is incomplete.  Use @kbd{Linefeed} to insert line
breaks in the middle of a form.

This command uses @code{Unwedge Interactive Input Confirm} in the same way
@code{Confirm Interactive Input} does.
@end deffn

@deffn Command "Abort Eval Input", 
        stuff (bound to @strong{M-i} in @code{Eval} mode)]
This command moves the the end of the buffer and prompts, ignoring any
input already typed in.
@end deffn


@subsection Error Handling
@cindex error handling 
When an error happens inside of Xomax, Xomax will trap the error and
display the error message in the echo area, possibly along with the
"@code{Internal error:}" prefix.  If you want to debug the error, type @strong{?}.
This causes the prompt "@code{Debug:}" to appear in the echo area.  The following
commands are recognized:

@table @dfn
@item @strong{d}
Enter a break-loop so that you can use the Lisp debugger.
Proceeding with "@code{go}" will reenter Xomax and give the "@code{Debug:}"
prompt again.

@item @strong{e}
Display the original error message in a pop-up window.

@item @strong{b}
Show a stack backtrace in a pop-up window.

@item @strong{q, Escape}
Quit from this error to the nearest command loop.

@item @strong{r}
Display a list of the restart cases and prompt for the number of a
@code{restart-case} with which to continue.  Restarting may result in prompting in
the window in which Lisp started.

@end table

Only errors within the editor process are handled in this way.  Errors during
eval server operations are handled using normal terminal I/O on a typescript in
the eval server's slave buffer or background buffer (see page
@xref{operations}).  Errors due to interaction in a slave buffer will cause
the debugger to be entered in the slave buffer.


@section Command Line Switches
@cindex slave-switch 
Two command line switches control the initialization of editor and eval servers
for a Lisp process:

@table @dfn

@item @code{-edit}
@cindex edit-switch 
This switch starts up Xomax.  If there is a non-switch command line word
immediately following the program name, then the system interprets it as a file
to edit.  For example, given
@example
lisp file.txt -edit
@end example
Lisp will go immediately into Xomax finding the file @code{file.txt}.

@item @f<-slave [>@i[name]@f<]>
This switch causes the Lisp process to become a slave of the editor process
@i[name].  An editor Lisp determines @i[name] when it allows connections from
slaves.  Once the editor chooses a name, it keeps the same name until the
editor's Lisp process terminates.  Since the editor can automatically create
slaves on its own machine, this switch is useful primarily for creating slaves
that run on a different machine.  @code{hqb}'s machine is @code{ME.CS.CMU.EDU}, and
he wants want to run a slave on @code{SLAVE.CS.CMU.EDU}, then he should use the
@code{Accept Slave Connections} command, telnet to the machine, and invoke Lisp
supplying @code{-slave} and the editor's name.  The command displays the editor's
name.

@end table
