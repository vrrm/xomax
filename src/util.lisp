(in-package :hemlock-internals)


(defparameter *trace-output-file* "trace-output.log")

(defparameter *packages-to-trace*
  (list :hemlock
	:hemlock-interface
	:hemlock-internals
	:hemlock-user
	:hemlock.frame-buffer
	:hemlock.terminfo
	:xlib))

(defparameter *no-trace-functions*
  (list 'hemlock-internals::bitmap-hunk-font-family
	'hemlock-internals::mark))

(defun get-all-functions-in-packages (&optional package)
  (reduce (lambda (all-functions functions-in-package)
	    (if functions-in-package
		(concatenate 'list all-functions functions-in-package)
		all-functions))
	  (mapcar (lambda (symbol) (get-all-functions-in-package symbol))
		  *packages-to-trace*)))

(defun get-all-functions-in-package (package)
  (let ((lst ())
        (package (find-package package)))
    (do-symbols (s (find-package package) lst)
      (multiple-value-bind (s symbol-type) (find-symbol (symbol-name s) package)
	(when (and (fboundp s)
		   (not (macro-function s))
		   (not (eql symbol-type :inherited))
		   )
	  (if package
	      (when (eql (symbol-package s) package)
		(push s lst))
	      (push s lst))))
      lst)))

;; (defun trace-function (function-symbol)
;;   (eval `(trace ,(symbol-function 'reverse-find-attribute))))


(defun trace-specified-functions ()
  (when *trace-output-file*
    (setf *trace-output* (open "trace-output.log"
			       :direction :output
			       :if-exists :append
			       :if-does-not-exist :create))
    (trace-or-untrace-specified-functions :trace)))

(defun trace-or-untrace-specified-functions (&optional (trace-or-untrace :trace))
  (let ((functions (get-all-functions-in-packages)))
    (dolist (f *no-trace-functions*)
      (setf functions (remove f functions)))
    (dolist (f (get-all-functions-in-packages))
      (unwind-protect
	   (progn
	     (format t "Tracing ~s~%" f)
	     (trace-function f trace-or-untrace))
	(force-output)))))

;; (setf fd (open "/home/rett/dev/xomax/src/output.txt" :direction :output :if-exists :overwrite :if-does-not-exist :create))
;; (setf *trace-output* fd)

;; :reverse-find-attribute
;; :REVERSE-FIND-ATTRIBUTE

;; (find-symbol "xomax" (find-package (symbol-name :hemlock-internals)))

(defun trace-function (function-symbol &optional (trace-or-untrace :trace))
  ;; switching to some package we know that the symbol wasn't defined
  ;; in, forces lisp to make the symbol fully qualified.
  (let* ((symbol-package (symbol-package function-symbol))
	 (old-package (package-name *package*))
	 (*package* (find-package (symbol-name :cl-user))))
    ;; After this point apperently function-symbol no longer
    ;; 'remembers' what package it's in, so look it up.
    (let* ((function-name (symbol-name function-symbol))
	   (refound-symbol (find-symbol function-name symbol-package)))
      ;; the only way to call trace with an arbitrary funcion is by
      ;; injecting a fully qualified symbol.
      (case trace-or-untrace
	(:trace
	 (eval `(trace ,refound-symbol)))
	(:untrace
	 (eval `(untrace ,refound-symbol)))))))



(defun my-function1 () )

(defparameter functions-to-trace '(my-function1 my-function2 ;; ....
				   ))
;; (
;;  (dolist (the-function functions-to-trace)
;;    (trace the-function))
