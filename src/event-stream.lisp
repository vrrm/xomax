(in-package :hemlock-internals)

(defun str (&rest strings)
  (apply #'concatenate (cons 'string strings)))

(defclass readable-writable-stream
    (trivial-gray-streams:fundamental-binary-input-stream
     trivial-gray-streams:fundamental-binary-output-stream
     trivial-gray-streams:fundamental-character-input-stream
     trivial-gray-streams:fundamental-character-output-stream)
  ((stream-buffer :accessor stream-buffer
		  :initform (make-instance 'chanl:unbounded-channel))
   (last-read :initform nil :accessor last-read)
   (last-read-current :initform nil)))

(defun create-readable-writable-stream ()
  (make-instance 'readable-writable-stream))

(defmethod stream-read-char ((stream readable-writable-stream))
  (with-slots (stream-buffer last-read last-read-current) stream
    (cond (last-read-current
	   (setf last-read-current nil)
	   last-read)
	  (t
	   (let ((char (chanl:recv stream-buffer :blockp nil)))
	     (cond ((not char) nil)
		   ((eq char :eof)
		    (setf last-read nil)
		    :eof)
		   (t (setf last-read char)
		      last-read)))))))

(defmethod stream-unread-char ((stream readable-writable-stream) c)
  (with-slots (stream-buffer last-read last-read-current) stream
    (cond (last-read-current
	   (error "Unable to unread character.  Current character on stream is already unread."))
	  ((null last-read)
	   (error "No character has been read from the stream yet."))
	  ((not (eql last-read c))
	   (error (str "Unread characters need to to match the character that was actually read.")))
	  (t
	   (setf last-read-current t)))))

(defmethod stream-write-char ((stream readable-writable-stream) c)
  (with-slots (stream-buffer) stream
    (chanl:send stream-buffer c)))

(defmethod stream-read-char-no-hang ((stream readable-writable-stream))
    ;; Current implementation can't hang, so simply use read
    ;; character.
    (stream-read-char stream))

(defmethod stream-listen ((stream readable-writable-stream))
  (error "Method not implemented."))

(defmethod stream-read-line ((stream readable-writable-stream))
  (error "Method not implemented."))

(defmethod stream-clear-input ((stream readable-writable-stream))
  (error "Method not implemented."))

(defmethod stream-line-column ((stream readable-writable-stream))
  (error "Method not implemented."))

(defmethod stream-start-line-p ((stream readable-writable-stream))
  (error "Method not implemented."))

(defmethod stream-write-string ((stream readable-writable-stream) str &optional start end)
  (when (and start end (or (>= start end) (> start (length str))))
    (error "start and end parameters not correct."))
  (with-slots (stream-buffer last-read) stream
    (loop for i from 0 to (1- (length str))
       do (write-char (aref str i) stream))))

(defmethod stream-terpri ((stream readable-writable-stream))
  (with-slots (stream-buffer last-read) stream
    (vector-push-extend #\Newline stream-buffer)))

(defmethod stream-finish-output ((stream readable-writable-stream))
  ;; nop
  )

(defmethod stream-force-output ((stream readable-writable-stream))
  ;; nop
  )

(defmethod stream-clear-output ((stream readable-writable-stream))
  ;;nop
  )

(defmethod stream-read-byte ((stream readable-writable-stream))
  (stream-read-char stream))

(defmethod stream-write-byte ((stream readable-writable-stream) byte)
    (stream-write-char stream byte))

(defmethod stream-read-sequence ((stream readable-writable-stream) seq start end &key)
  (with-slots (stream-buffer last-read) stream
    (do* ((outpos start (1+ outpos))
	  (char (stream-read-char stream)
		(stream-read-char stream)))
	     ((or (= outpos end)
		  (not char)
		  (eq char :eof))
	      outpos)
      (setf (elt seq outpos)
	    char))))

(defmethod stream-write-sequence ((s readable-writable-stream) seq start end &key)
  (error "Method not implemented."))

(defmethod stream-file-position ((s readable-writable-stream))
  nil
  )

(defmethod (setf stream-file-position) (newval (s readable-writable-stream))
  nil
  )

(defun my-slurp (stream)
  (let ((str '()))
  (do ((char (read-char stream nil nil)
	     (read-char stream nil nil)))
      ((not char)
       (coerce str 'string))
    (push char str))))



;; (defparameter output-vector
;;       (make-stream-vector :element-type '(usigned-byte 8)))

;; (defparameter vector-output-stream
;;   (make-instance 'vector-output-stream :vector output-vector))

;; (defparameter in-memory-input-stream
;;       (vector-input-stream output-vector))

;; (write-byte 4 vector-output-stream)
;; > 4

;; (read-byte in-memory-input-stream)
;; > 4


;; (chanl:send channel event)))
;; (make-instance 'chanl:unbounded-channel)))


(defun test-readable-writable-stream ()
  (let* ((rws (create-readable-writable-stream))
	 (c #\d))
    (stream-write-char rws c)
    (eq c (stream-read-char rws))))
