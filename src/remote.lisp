;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;
;;; **********************************************************************
;;;
;;; This file implements a simple remote procedure call mechanism on
;;; top of wire.lisp.
;;;
;;; Written by William Lott.
;;;

(in-package :wire)

(defstruct remote-wait value1  value2  value3  value4  value5 abort finished)

(defvar *pending-returns* nil "A list of wires, remote-wait structs")

;;; maybe-nuke-remote-wait -- internal
;;;
;;; If the remote wait has finished, remove the external translation.
;;; Otherwise, mark the remote wait as finished so the next call to
;;; maybe-nuke-remote-wait will really nuke it.

(defun maybe-nuke-remote-wait (remote)
  (cond ((remote-wait-finished remote) (forget-remote-translation remote) t)
        (t (setf (remote-wait-finished remote) t) nil)))

;;; remote -- public
;;;
;;; Execute the body remotely. Subforms are executed locally in the
;;; lexical environment of the macro call. No values are returned.

(defmacro remote (wire-form &body forms)
  "Evaluates the given forms remotly. No values are returned, as the
   remote evaluation is asyncronous."
  (let ((wire (gensym)))
    `(let ((,wire ,wire-form))
       ,@(mapcar #'(lambda (form)
		     `(wire-output-funcall ,wire
					   ',(car form)
					   ,@(cdr form)))
                 forms)
       (values))))

;;; remote-value-bind -- public
;;;
;;; Send to remote forms. First, a call to the correct dispatch
;;; routine based on the number of args, then the actual call. The
;;; dispatch routine will get the second funcall and fill in the
;;; correct number of arguments.  Note: if there are no arguments, we
;;; don't even wait for the function to return, cause we can kind of
;;; guess at what the currect results would be.

(defmacro remote-value-bind (wire-form vars form &rest body)
  "Bind vars to the multiple values of FORM (which is executed
   remotely). The forms in BODY are only executed if the remote
   function returned (as apposed to aborting due to a throw)."
  (cond
    ((null vars)
     `(progn
        (remote ,wire-form ,form)
        ,@body))
    (t
     (let ((remote (gensym))
           (wire (gensym)))
       `(let* ((,remote (make-remote-wait))
               (,wire ,wire-form)
               (*pending-returns* (cons (cons ,wire ,remote)
                                        *pending-returns*)))
          (unwind-protect
               (let ,vars
                 (remote ,wire
                   (,(case (length vars)
                           (1 'do-1-value-call)
                           (2 'do-2-value-call)
                           (3 'do-3-value-call)
                           (4 'do-4-value-call)
                           (5 'do-5-value-call)
                           (t 'do-n-value-call))
                     (make-remote-object ,remote))
                   ,form)
                 (wire-force-output ,wire)
                 (loop
                    (sb-sys:serve-all-events)
                    (when (remote-wait-finished ,remote)
                      (return)))
                 (unless (remote-wait-abort ,remote)
                   ,(case (length vars)
                          (1 `(setf ,(first vars) (remote-wait-value1 ,remote)))
                          (2 `(setf ,(first vars) (remote-wait-value1 ,remote)
                                    ,(second vars) (remote-wait-value2 ,remote)))
                          (3 `(setf ,(first vars) (remote-wait-value1 ,remote)
                                    ,(second vars) (remote-wait-value2 ,remote)
                                    ,(third vars) (remote-wait-value3 ,remote)))
                          (4 `(setf ,(first vars) (remote-wait-value1 ,remote)
                                    ,(second vars) (remote-wait-value2 ,remote)
                                    ,(third vars) (remote-wait-value3 ,remote)
                                    ,(fourth vars) (remote-wait-value4 ,remote)))
                          (5 `(setf ,(first vars) (remote-wait-value1 ,remote)
                                    ,(second vars) (remote-wait-value2 ,remote)
                                    ,(third vars) (remote-wait-value3 ,remote)
                                    ,(fourth vars) (remote-wait-value4 ,remote)
                                    ,(fifth vars) (remote-wait-value5 ,remote)))
                          (t
                           (do ((remaining-vars vars (cdr remaining-vars))
                                (form (list 'setf)
                                      (nconc form
                                             (list (car remaining-vars)
                                                   `(pop values)))))
                               ((null remaining-vars)
                                `(let ((values (remote-wait-value1 ,remote)))
                                   ,form)))))
                   ,@body))
            (maybe-nuke-remote-wait ,remote)))))))


;;; REMOTE-VALUE -- public
;;;
;;; Alternate interface to getting the single return value of a remote
;;; function. Works pretty much just the same, except the single value is
;;; returned.

(defmacro remote-value (wire-form form &optional
                                         (on-server-unwind
                                          `(error  "Remote server unwound")))
  "Execute the single form remotly. The value of the form is returned.
  The optional form on-server-unwind is only evaluated if the server unwinds
  instead of returning."
  (let ((remote (gensym))
	(wire (gensym)))
    `(let* ((,remote (make-remote-wait))
	    (,wire ,wire-form)
	    (*pending-returns* (cons (cons ,wire ,remote)
				     *pending-returns*)))
       (unwind-protect
            (progn
              (remote ,wire
                (do-1-value-call (make-remote-object ,remote))
                ,form)
              (wire-force-output ,wire)
              (loop
                 (sb-sys:serve-all-events)
                 (when (remote-wait-finished ,remote)
                   (return))))
	 (maybe-nuke-remote-wait ,remote))
       (if (remote-wait-abort ,remote)
           ,on-server-unwind
           (remote-wait-value1 ,remote)))))

;;; define-functions -- internal
;;;
;;; Defines two functions, one that the client runs in the server, and
;;; one that the server runs in the client:
;;;
;;; do-n-value-call -- internal
;;;
;;; Executed by the remote process. Reads the next object off the wire and
;;; sends the value back. Unwind-protect is used to make sure we send something
;;; back so the requestor doesn't hang.
;;;
;;; return-n-value -- internal
;;;
;;; The remote procedure returned the given value, so fill it in the
;;; remote-wait structure. Note, if the requestor has aborted, just throw
;;; the value away.

(defmacro define-functions (values)
  (let ((do-call (intern (format nil "~A~D~A" '#:do- values '#:-value-call)))
        (return-values (intern (format nil "~A~D~A~P" '#:return- values
                                       `#:-value values)))
        (vars nil))
    (dotimes (i values)
      (push (gensym) vars))
    (setf vars (nreverse vars))
    `(progn
       (defun ,do-call (result)
         (let (worked ,@vars)
           (unwind-protect
                (progn
                  (multiple-value-setq ,vars
                    (wire-get-object *current-wire*))
                  (setf worked t))
             (if worked
                 (remote *current-wire*
                   (,return-values result ,@vars))
                 (remote *current-wire*
                   (remote-return-abort result)))
             (wire-force-output *current-wire*))))
       (defun ,return-values (remote ,@vars)
         (let ((result (remote-object-value remote)))
           (unless (maybe-nuke-remote-wait result)
             ,@(let ((setf-forms nil))
                    (dotimes (i values)
                      (push `(setf (,(intern (format nil
                                                     "~A~D" '#:remote-wait-value
                                                     (1+ i)))
                                     result)
                                   ,(nth i vars))
                            setf-forms))
                    (nreverse setf-forms))))
         nil))))

(define-functions 1)
(define-functions 2)
(define-functions 3)
(define-functions 4)
(define-functions 5)

;;; invoke-with-wire-and-remote -- public
;;;
;;; Alternate interface to getting the single return value of a remote
;;; function. Works pretty much just the same, except the single value is
;;; returned.

(defun invoke-with-wire-and-remote (wire fun &optional on-server-unwind)
  "Execute the single form remotly. The value of the form is returned.
  The optional form on-server-unwind is only evaluated if the server unwinds
  instead of returning."
  (let* ((remote (make-remote-wait))
         (*pending-returns* (cons (cons wire remote)
                                  *pending-returns*)))
    (unwind-protect
         (progn
           (funcall fun wire remote)
           (wire-force-output wire)
           (loop
              (wire-get-object wire)
              (when (remote-wait-finished remote)
                (return))))
      (maybe-nuke-remote-wait remote))
    (cond
      ((not (remote-wait-abort remote))
       (remote-wait-value1 remote))
      (on-server-unwind
       (funcall on-server-unwind))
      (t
       (error "Remote server unwound")))))

(defmacro remote-value (wire-form form &optional on-server-unwind)
  "Execute the single form remotely. The value of the form is returned.
  The optional form on-server-unwind is only evaluated if the server unwinds
  instead of returning."
  (let ((wire (gensym))
        (remote (gensym)))
    `(invoke-with-wire-and-remote
      ,wire-form
      (lambda (,wire ,remote)
        (remote ,wire
          (do-1-value-call (make-remote-object ,remote))
          ,form))
      ,@(when on-server-unwind
              `((lambda ()
                  ,on-server-unwind))))))

;;; do-n-value-call -- internal
;;;
;;; For more values then 5, all the values are rolled into a list and
;;; passed back as the first value, so we use RETURN-1-VALUE to return
;;; it.

(defun do-n-value-call (result)
  (let (worked values)
    (unwind-protect
         (progn
           (setf values
                 (multiple-value-list (wire-get-object *current-wire*)))
           (setf worked t))
      (if worked
          (remote *current-wire*
            (return-1-value result values))
          (remote *current-wire*
            (remote-return-abort result)))
      (wire-force-output *current-wire*))))

;;; remote-return-abort -- internal
;;;
;;; The remote call aborted instead of returned.

(defun remote-return-abort (result)
  (setf result (remote-object-value result))
  (unless (maybe-nuke-remote-wait result)
    (setf (remote-wait-abort result) t)))

;;; serve-requests -- internal
;;;
;;; Serve all pending requests on the given wire.

(defun serve-requests (wire on-death)
  (handler-bind
      ((wire-eof #'(lambda (condition)
		     (declare (ignore condition))
		     (sb-sys:invalidate-descriptor (wire-fd wire))
		     (sb-unix:unix-close (wire-fd wire))
		     (dolist (pending *pending-returns*)
		       (when (eq (car pending)
				 wire)
			 (unless (maybe-nuke-remote-wait (cdr pending))
			   (setf (remote-wait-abort (cdr pending))
				 t))))
		     (when on-death
		       (funcall on-death))
		     (return-from serve-requests (values))))
       (wire-error #'(lambda (condition)
		       (declare (ignore condition))
		       (sb-sys:invalidate-descriptor (wire-fd wire)))))
    (loop
       (unless (wire-listen wire)
         (return))
       (wire-get-object wire)))
  (values))
;;; Registry of source and (targets?) for events
(defvar *remote-server-event-base*)

;;; NEW-CONNECTION -- internal
;;;
;;; Maybe build a new wire and add it to the servers list of fds. If
;;; the user Supplied a function, close the socket if it returns
;;; NIL. Otherwise, install the wire.
(defun new-connection (server-socket addr on-connect)
  (let ((wire (make-wire server-socket))
	(on-death nil))
    (if (or (null on-connect)
	    (multiple-value-bind (okay death-fn)
                (funcall on-connect wire addr)
	      (setf on-death death-fn)
	      okay))
        (iolib:set-io-handler
         *remote-server-event-base*
         socket
         :read (lambda (server-socket event error)
                 (declare (ignore event))
                 (when (eq error :error) (error "error with ~A" server-socket))
                 (serve-requests wire on-death)))
        (close server-socket))))

;;; REQUEST-SERVER structure
;;;
;;; Just a simple handle on the socket and system:serve-event handler that make
;;; up a request server.
;;;
(defstruct (request-server
             (:print-function %print-request-server))
  socket
  handler)

(defun %print-request-server (rs stream depth)
  (declare (ignore depth))
  (print-unreadable-object (rs stream :type t)
    (format stream "for ~D" (request-server-socket rs))))

;;;(ql:quickload 'iolib.examples)

(defun listen-on-socket (socket)
  ;; Convert to a listening socket
  (iolib:listen-on socket :backlog 10000)
  (format t "Listening on socket bound to: ~A:~A~%"
          (iolib:local-host socket) (iolib:local-port socket)))

;;; CREATE-REQUEST-SERVER -- Public.
;;;
;;; Create a TCP/IP listener on the given port.  If anyone tries to connect to
;;; it, call NEW-CONNECTION to do the connecting.
;;;
(defun create-request-server (port  &optional on-connect &key reuse-address)
  "Create a request server on the given port.  Whenever anyone connects to it,
   call the given function with the newly created wire and the address of the
   connector.  If the function returns NIL, the connection is destroyed;
   otherwise, it is accepted.  This returns a manifestation of the server that
   DESTROY-REQUEST-SERVER accepts to kill the request server."
  (let ((socket (iolib:make-socket
                 :connect :passive
                 :address-family :internet
                 :type :stream
                 :external-format '(:utf-8 :eol-style :crlf)
                 :ipv6 nil)))
    (format t "Created socket: ~A[fd=~A]~%" socket (iolib:socket-os-fd socket))
    ;; Bind the socket to all interfaces with specified port.
    (iolib:bind-address socket iolib:+ipv4-unspecified+ ; which means INADDR_ANY or 0.0.0.0
                  :port port
                  :reuse-addr t)
    (format t "Bound socket: ~A~%" socket)
    (listen-on-socket socket)
    socket))

(defun run-server (server-socket)
    ;; Block on accepting a connection
    (format t "Waiting to accept a connection...~%")
    (let ((client (iolib:accept-connection server-socket :wait t)))
      (when client
        ;; When we get a new connection, show who it is from.
        (multiple-value-bind (who rport)
            (iolib:remote-name client)
          (format t "Got a connection from ~A:~A!~%" who rport))
        ;; Since we're using a internet TCP stream, we can use format
        ;; with it. However, we should be sure to call finish-output on
        ;; the socket in order that all the data is sent. Also, this is
        ;; a blocking write.
        (multiple-value-bind (s m h d mon y)
            (iolib.examples::get-decoded-time)
          (format t "Sending the time...")
          (format client "~A/~A/~A ~A:~A:~A~%" mon d y h m s)
          (iolib.examples::finish-output client))
        ;; We're done talking to the client.
        (close client)
        (format t "Sent!~%"))
      ;; We're done with the server socket too.
      (close server-socket)
      (iolib.examples::finish-output)
      t))


;;; CONNECT-TO-REMOTE-SERVER -- Public.
;;;
;;; Just like the doc string says, connect to a remote server. A handler is
;;; installed to handle return values, etc.
;;;
(defun connect-to-remote-server (host port &optional on-death)
  ;; Create a internet TCP socket under IPV4
  (let ((socket (iolib:make-socket
                 :connect :active
                 :address-family :internet
                 :type :stream
                 :external-format '(:utf-8 :eol-style :crlf)
                 :ipv6 nil)))
    ;; do a blocking connect to the daytime server on the port.
    (iolib:connect socket host :port port :wait t)
    (let ((wire (make-wire socket)))
      (iolib:set-io-handler
       *remote-server-event-base*
       socket
       :read (lambda (socket event error)
               (declare (ignore event))
               (when (eq error :error) (error "error with ~A" socket))
               (serve-requests wire on-death)))
      wire)))

;;; DESTROY-REQUEST-SERVER -- Public.
;;;
;;; Removes the request server from SERVER's list of file descriptors and
;;; closes the socket behind it.
;;;
(defun destroy-request-server (server-socket)
  "Quit accepting connections to the given request server."
  (iolib:remove-fd-handlers *remote-server-event-base* server-socket :read t)
  (close server-socket)
  nil)

(defun test-run-server-async ()
  (let* ((port 9995)
         (s (create-request-server port))
         (host iolib:+ipv4-unspecified+))
    (sb-thread:make-thread (lambda () (run-server s)))
    (format t "All threads~%")
    (connect-to-remote-server host port)
    (sb-thread:list-all-threads)))

(defun test-run-server-sync ()
  (let* ((port 9995)
         (s (create-request-server port))
         (host iolib:+ipv4-unspecified+)
         (thread (sb-thread:make-thread (lambda () (run-server s)))))
    (format t "run: (connect-to-remote-server ~A ~A) ~%"  host port)
    thread))
