;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :cl-user)

;; Note: I want real relative package names like the Symbolics has
;; them. In the mean time:

(defpackage :debug-internals
  (:use :common-lisp
        :command-line-arguments
        :iterate)
  (:nicknames :di)
  (:export #:*debugging-interpreter*
	   #:activate-breakpoint
	   #:ambiguous-debug-variables
	   #:ambiguous-variable-name
	   #:breakpoint
	   #:breakpoint-active-p
	   #:breakpoint-hook-function
	   #:breakpoint-info
	   #:breakpoint-kind
	   #:breakpoint-p
	   #:breakpoint-what
	   #:code-location
	   #:code-location-debug-block
	   #:code-location-debug-function
	   #:code-location-debug-source
	   #:code-location-form-number
	   #:code-location-p
	   #:code-location-top-level-form-offset
	   #:code-location-unknown-p
	   #:code-location=
	   #:deactivate-breakpoint
	   #:debug-block
	   #:debug-block-elsewhere-p
	   #:debug-block-p
	   #:debug-block-successors
	   #:debug-condition
	   #:debug-error
	   #:debug-function
	   #:debug-function-function
	   #:debug-function-kind
	   #:debug-function-lambda-list
	   #:debug-function-name
	   #:debug-function-p
	   #:debug-function-start-location
	   #:debug-function-symbol-variables
	   #:debug-source
	   #:debug-source-compiled
	   #:debug-source-created
	   #:debug-source-from
	   #:debug-source-name
	   #:debug-source-p
	   #:debug-source-root-number
	   #:debug-source-start-positions
	   #:debug-variable
	   #:debug-variable-id
	   #:debug-variable-info-available
	   #:debug-variable-name
	   #:debug-variable-p
	   #:debug-variable-package
	   #:debug-variable-symbol
	   #:debug-variable-valid-value
	   #:debug-variable-validity
	   #:debug-variable-value
	   #:delete-breakpoint
	   #:delete-breakpoint-for-editor
	   #:do-blocks
	   #:do-debug-block-locations
	   #:do-debug-function-blocks
	   #:do-debug-function-variables
	   #:eval-in-frame
	   #:form-number-translations
	   #:frame
	   #:frame-catches
	   #:frame-code-location
	   #:frame-debug-function
	   #:frame-down
	   #:frame-function-mismatch
	   #:frame-number
	   #:frame-p
	   #:frame-up
	   #:function-debug-function
	   #:function-end-cookie-valid-p
	   #:invalid-control-stack-pointer
	   #:invalid-value
	   #:lambda-list-unavailable
	   #:make-breakpoint
	   #:no-debug-blocks
	   #:no-debug-function-returns
	   #:no-debug-info
	   #:preprocess-for-eval
	   #:return-from-frame
	   #:set-breakpoint-for-editor
	   #:set-location-breakpoint-for-editor
	   #:source-path-context
	   #:top-frame
	   #:unhandled-condition
	   #:unknown-code-location
	   #:unknown-code-location-p
	   #:unknown-debug-variable
	   #:code-location-kind
	   #:flush-frames-above
	   #:find-debug-tag-for-frame))

(defpackage :hemlock-interface
    (:use)
  (:export
   ;; Functions from the CIM:
   #:linep
   #:line-string
   #:line-previous
   #:line-next
   #:line-buffer
   #:line-length
   #:line-character
   #:line-plist
   #:line-signature
   #:markp
   #:mark-line
   #:mark-charpos
   #:mark-kind
   #:previous-character
   #:next-character
   #:mark
   #:copy-mark
   #:delete-mark
   #:move-to-position
   #:move-mark
   #:line-start
   #:line-end
   #:buffer-start
   #:buffer-end
   #:mark-before
   #:mark-after
   #:character-offset
   #:line-offset
   #:region
   #:regionp
   #:make-empty-region
   #:copy-region
   #:region-to-string
   #:string-to-region
   #:line-to-region
   #:region-start
   #:region-end
   #:region-bounds
   #:set-region-bounds
   #:count-lines
   #:count-characters
   #:check-region-query-size
   #:current-buffer
   #:current-point
   #:current-mark
   #:pop-buffer-mark
   #:push-buffer-mark
   #:change-to-buffer
   #:previous-buffer
   #:make-buffer
   #:bufferp
   #:buffer-name
   #:buffer-region
   #:buffer-pathname
   #:buffer-write-date
   #:buffer-point
   #:buffer-mark
   #:buffer-active-region-p
   #:buffer-start-mark
   #:buffer-end-mark
   #:buffer-writable
   #:buffer-modified
   #:buffer-signature
   #:buffer-variables
   #:buffer-modes
   #:buffer-windows
   #:buffer-delete-hook
   #:buffer-undo-p
   #:delete-buffer
   #:delete-buffer-if-possible
   #:make-modeline-field
   #:modeline-field-p
   #:modeline-field-name
   #:modeline-field
   #:modeline-field-function
   #:modeline-field-width
   #:buffer-modeline-fields
   #:buffer-modeline-field-p
   #:update-modeline-fields
   #:update-modeline-field
   #:insert-character
   #:insert-string
   #:insert-region
   #:ninsert-region
   #:delete-characters
   #:delete-region
   #:delete-and-save-region
   #:fetch-cut-string
   #:store-cut-string
   #:filter-region
   #:start-line-p
   #:end-line-p
   #:empty-line-p
   #:blank-line-p
   #:blank-before-p
   #:blank-after-p
   #:same-line-p
   #:mark<
   #:mark<=
   #:mark=
   #:mark/=
   #:mark>=
   #:mark>
   #:line<
   #:line<=
   #:line>=
   #:line>
   #:lines-related
   #:first-line-p
   #:last-line-p
   #:kill-region
   #:kill-characters
   #:activate-region
   #:deactivate-region
   #:region-active-p
   #:check-region-active
   #:current-region
   #:new-search-pattern
   #:search-pattern-p
   #:get-search-pattern
   #:find-pattern
   #:replace-pattern
   #:current-variable-tables
   #:defhvar
   #:variable-value
   #:variable-documentation
   #:variable-hooks
   #:variable-name
   #:string-to-variable
   #:hemlock-bound-p
   #:delete-variable
   #:make-command
   #:commandp
   #:command-documentation
   #:command-function
   #:command-name
   #:bind-key
   #:command-bindings
   #:delete-key-binding
   #:get-command
   #:map-bindings
   #:key-translation
   #:interactive
   #:last-command-type
   #:prefix-argument
   #:recursive-edit
   #:in-recursive-edit
   #:exit-recursive-edit
   #:abort-recursive-edit
   #:defmode
   #:mode-documentation
   #:buffer-major-mode
   #:buffer-minor-mode
   #:mode-variables
   #:mode-major-p
   #:defattribute
   #:character-attribute-name
   #:character-attribute-documentation
   #:character-attribute
   #:character-attribute-p
   #:shadow-attribute
   #:unshadow-attribute
   #:find-attribute
   #:reverse-find-attribute
   #:character-attribute-hooks
   #:current-window
   #:make-window
   #:windowp
   #:delete-window
   #:window-buffer
   #:window-display-start
   #:window-display-end
   #:window-display-recentering
   #:window-point
   #:center-window
   #:scroll-window
   #:displayed-p
   #:window-height
   #:window-width
   #:next-window
   #:previous-window
   #:mark-to-cursorpos
   #:cursorpos-to-mark
   #:last-key-event-cursorpos
   #:mark-column
   #:move-to-column
   #:show-mark
   #:redisplay
   #:redisplay-all
   #:editor-finish-output
   #:define-logical-key-event
   #:logical-key-event-key-events
   #:logical-key-event-p
   #:clear-echo-area
   #:message
   #:loud-message
   #:prompt-for-buffer
   #:prompt-for-key-event
   #:prompt-for-key
   #:prompt-for-file
   #:prompt-for-integer
   #:prompt-for-keyword
   #:prompt-for-expression
   #:prompt-for-string
   #:prompt-for-variable
   #:prompt-for-y-or-n
   #:prompt-for-yes-or-no
   #:process-file-options
   #:pathname-to-buffer-name
   #:buffer-default-pathname
   #:read-file
   #:write-file
   #:write-buffer-file
   #:read-buffer-file
   #:find-file-buffer
   ;;   #:ed
   #:exit-hemlock
   #:pause-hemlock
   #:get-key-event
   #:unget-key-event
   #:clear-editor-input
   #:listen-editor-input
   #:make-hemlock-output-stream
   #:hemlock-output-stream-p
   #:make-hemlock-region-stream
   #:hemlock-region-stream-p
   #:editor-error-format-string
   #:editor-error-format-arguments
   #:editor-error
   #:add-definition-dir-translation
   #:delete-definition-dir-translation
   #:schedule-event
   #:remove-scheduled-event
   #:in-lisp
   #:indent-region
   #:indent-region-for-commands
   #:delete-horizontal-space
   #:pre-command-parse-check
   #:form-offset
   #:top-level-offset
   #:mark-top-level-form
   #:defun-region
   #:inside-defun-p
   #:start-defun-p
   #:forward-up-list
   #:backward-up-list
   #:valid-spot
   #:defindent
   #:word-offset
   #:sentence-offset
   #:paragraph-offset
   #:mark-paragraph
   #:goto-page
   #:page-offset
   #:page-directory
   #:display-page-directory
   #:fill-region
   #:fill-region-by-paragraphs
   #:make-string-table
   #:string-table-p
   #:string-table-separator
   #:delete-string
   #:clrstring
   #:getstring
   #:complete-string
   #:find-ambiguous
   #:find-containing
   #:make-ring
   #:ringp
   #:ring-length
   #:ring-ref
   #:ring-push
   #:ring-pop
   #:rotate-ring
   #:save-for-undo
   #:make-region-undo
   #:supply-generic-pointer-up-function

   ;; Macros from the CIM:
   #:with-writable-buffer
   #:value
   #:setv
   #:add-hook
   #:remove-hook
   #:invoke-hook
   #:defcommand
   #:use-buffer
   #:command-case
   #:define-file-option
   #:define-file-type-hook
   #:do-active-group
   #:with-input-from-region
   #:with-output-to-mark
   #:with-pop-up-display
   #:handle-lisp-errors
   #:do-alpha-chars
   #:do-strings

   ;; Later, possibly ill-adviced additions
   #:goto-buffer-start
   #:goto-buffer-end

   ;;;; !!!!
   ;;;; !!!! Everything below here is because putting bit-screen.lisp,
   ;;;; !!!! bit-display.lisp and hunk-draw.lisp into its own package.
   ;;;; !!!! Besides the DEVICE-xyz entries this list should be empty.
   ;;;; !!!! --GB 2004-05-26
   ;;;; !!!!

   #:device                             ;[class]
   ;; their methods
   #:device-init
   #:device-exit
   #:device-smart-redisplay
   #:device-dumb-redisplay
   #:device-after-redisplay
   #:device-clear
   #:device-note-read-wait
   #:device-force-output
   #:device-finish-output
   #:device-put-cursor
   #:device-show-mark
   #:device-next-window
   #:device-previous-window
   #:device-delete-window
   #:device-random-typeout-full-more
   #:device-random-typeout-line-more
   #:device-random-typeout-setup
   #:device-random-typeout-cleanup
   #:device-beep
   ;;
   #:device-hunk
   #:random-typeout-stream
   #:with-mark
   #:init-bitmap-screen-manager
   #:reverse-video-hook-fun             ;### only defined in bitmap device, refered to from rompsite.lisp

   ;; variables, due to bit-screen.lisp
   #:*current-buffer*
   #:*current-window*
   #:*default-font-family*
   #:*echo-area-buffer*
   #:*hemlock-cursor*
   #:*random-typeout-buffers*
   #:*random-typeout-ml-fields*
   #:*window-list*
   #:child-interesting-xevents-mask
   #:group-interesting-xevents-mask
   #:random-typeout-xevents-mask
   #:the-sentinel
   ;; functions, due to bit-screen.lisp
   #:device-hunks
   #:random-typeout-stream-window
   #:window-group-height
   #:window-group-width
   #:device-hunk-device
   #:device-hunks
   #:font-family-cursor-y-offset
   #:font-family-height
   #:font-family-map
   #:font-family-width
   #:get-hemlock-cursor
   #:get-hemlock-grey-pixmap
   #:hemlock-window
   #:hlet
   #:make-black-color
   #:make-white-color
   #:make-window-group
   #:random-typeout-stream-mark
   #:random-typeout-stream-window
   #:remove-xwindow-object
   #:window-for-hunk
   #:window-group-height
   #:window-group-width
   #:window-group-xparent
   #:window-hunk
   #:window-input-handler
   #:window-modeline-buffer
   #:windowed-monitor-p
   ;;;; grrr
   #:bitmap-device-display
   #:bitmap-hunk-font-family
   #:bitmap-hunk-modeline-dis-line
   #:bitmap-hunk-modeline-pos
   #:bitmap-hunk-trashed
   #:bitmap-hunk-window
   #:bitmap-hunk-window-group
   #:bitmap-hunk-xwindow
   #:changed-bit
   #:default-font
   #:define-window-cursor
   #:*delete-window-hook*
   #:dis-line-chars
   #:dis-line-delta
   #:dis-line-flags
   #:dis-line-font-changes
   #:dis-line-length
   #:dis-line-position
   #:do-tty-full-more
   #:*echo-area-window*
   #:font-change-font
   #:font-change-next
   #:font-change-x
   #:font-family-baseline
   #:font-family-cursor-height
   #:font-family-cursor-width
   #:font-family-cursor-x-offset
   #:hunk-width-limit
   #:line-buffered-p
   #:make-xwindow-like-hwindow
   #:minimum-window-columns
   #:minimum-window-lines
   #:*more-prompt-action*
   #:moved-bit
   #:prepare-window-for-redisplay
   #:raise-echo-area-when-modified
   #:random-typeout-redisplay
   #:random-typeout-stream-first-more-p
   #:random-typeout-stream-more-mark
   #:random-typeout-stream-no-prompt
   #:set-window-name-for-buffer-name
   #:set-window-name-for-window-buffer
   #:unaltered-bits
   #:update-tty-line-buffered-stream
   #:wait-for-more
   #:window-first-changed
   #:window-first-line
   #:window-group
   #:window-group-p
   #:window-last-changed
   #:window-last-line
   #:window-modeline-dis-line
   #:window-old-lines
   #:dummy-line
   #:setup-modeline-image
   #:tick
   #:dis-line-old-chars
   #:update-window-image
   #:make-window-dis-line
   #:internal-make-window
   #:maybe-recenter-window
   #:window-modeline-buffer-len
   #:window-tick
   #:window-spare-lines
   #:maybe-recenter-window
   #:window                             ;as a type

   #:editor-input
   #:window-%buffer
   #:window-old-start
   #:device-name
   #:device-bottom-window-base
   #:device-hunks
   #:device-hunk-window
   #:device-hunk-position
   #:device-hunk-height
   #:device-hunk-next
   #:device-hunk-previous
   #:device-hunk-device

   #:list-all-connections
   #:connection
   #:connection-name
   #:connection-buffer
   #:connection-sentinel
   #:connection-filter
   #:connection-encoding
   #:delete-connection
   #:connection-listen
   #:connection-write
   #:tcp-connection
   #:make-tcp-connection
   #:process-connection
   #:connection-command
   #:make-process-connection
   #:make-pipelike-connection
   #:make-process-with-pty-connection
   #:connection-exit-status
   #:connection-exit-code
   #:file-connection
   #:connection-port
   #:connection-host
   #:make-file-connection
   #:conection-filename
   #:descriptor-connection
   #:connection-descriptor
   #:make-descriptor-connection
   #:listening-connection
   #:make-tcp-listener
   #:make-connection-device)
  (:import-from :hemlock.wire #:dispatch-events #:dispatch-events-no-hang)
  (:export #:dispatch-events #:dispatch-events-no-hang))

(defpackage :hemlock.frame-buffer
  (:use :common-lisp
        :hemlock-interface
        :command-line-arguments
        :iterate)
  (:export)
  (:import-from :hemlock.wire #:dispatch-events #:dispatch-events-no-hang))

(defpackage :hemlock-internals
  (:use :common-lisp
        :hemlock-interface
        :command-line-arguments
        :iterate)
  (:nicknames :hi)
  (:use :trivial-gray-streams)

  (:export
   #:*fast*                             ;hmm not sure about this one

   :*xdk-display*
   :key-event-char

   ;; rompsite.lisp
   #:show-mark #:*input-transcript* #:fun-defined-from-pathname
   #:editor-describe-function #:pause-hemlock #:store-cut-string
   #:fetch-cut-string #:schedule-event #:remove-scheduled-event
   #:enter-window-autoraise #:directoryp #:merge-relative-pathnames
   ;;
   ;; Export default-font to prevent a name conflict that occurs due to
   ;; the Hemlock variable "Default Font" defined in SITE-INIT below.
   ;;
   #:default-font
   #:*beep-function* #:beep

   ;;
   #:mark #:mark-line #:mark-charpos #:markp #:region #:region-start #:region-end
   #:regionp #:buffer #:bufferp #:buffer-modes #:buffer-point #:buffer-writable
   #:buffer-delete-hook #:buffer-windows #:buffer-variables #:buffer-write-date
   #:region #:regionp #:region-start #:region-end #:window #:windowp #:window-height
   #:window-width #:window-display-start #:window-display-end #:window-point
   #:window-display-recentering #:commandp #:command #:command-function
   #:command-documentation #:modeline-field #:modeline-field-p

   ;; from input.lisp
   #:get-key-event #:unget-key-event #:clear-editor-input #:listen-editor-input
   #:*last-key-event-typed* #:*key-event-history*
   #:input-waiting #:last-key-event-cursorpos

   ;; from macros.lisp
   #:invoke-hook #:value #:setv #:hlet #:string-to-variable #:add-hook #:remove-hook
   #:defcommand #:with-mark #:use-buffer #:editor-error #:canonical-case
   #:editor-error-format-string #:editor-error-format-arguments #:do-strings
   #:command-case #:reprompt #:with-output-to-mark #:with-input-from-region
   #:handle-lisp-errors #:with-pop-up-display #:*random-typeout-buffers*

   ;; from line.lisp
   #:line #:linep #:line-previous #:line-next #:line-plist #:line-signature

   ;; from ring.lisp
   #:ring #:ringp #:make-ring #:ring-push #:ring-pop #:ring-length #:ring-ref
   #:rotate-ring

   ;; from table.lisp
   #:string-table #:string-table-p #:make-string-table
   #:string-table-separator #:getstring
   #:find-ambiguous #:complete-string #:find-containing
   #:delete-string #:clrstring #:do-strings

   ;; bit-display.lisp
   #:redisplay #:redisplay-all

   ;; bit-screen.lisp
   #:make-xwindow-like-hwindow          ;used in input.lisp
   #:*delete-window-hook*

   ;; buffer.lisp
   #:buffer-modified #:buffer-region #:buffer-name #:buffer-pathname
   #:buffer-major-mode #:buffer-minor-mode #:buffer-modeline-fields
   #:buffer-modeline-field-p #:current-buffer #:current-point
   #:in-recursive-edit #:exit-recursive-edit #:abort-recursive-edit
   #:recursive-edit #:defmode #:mode-major-p #:mode-variables #:mode-documentation
   #:make-buffer #:delete-buffer #:with-writable-buffer #:buffer-start-mark
   #:buffer-end-mark #:*buffer-list*

   ;; charmacs.lisp
   #:do-alpha-chars

   ;; cursor.lisp
   #:mark-to-cursorpos #:center-window #:displayed-p #:scroll-window
   #:mark-column #:cursorpos-to-mark #:move-to-column

   ;; display.lisp
   #:redisplay #:redisplay-all

   ;; echo.lisp
   #:*echo-area-buffer* #:*echo-area-stream* #:*echo-area-window*
   #:*parse-starting-mark* #:*parse-input-region*
   #:*parse-verification-function* #:*parse-string-tables*
   #:*parse-value-must-exist* #:*parse-default* #:*parse-default-string*
   #:*parse-prompt* #:*parse-help* #:clear-echo-area #:message #:loud-message
   #:prompt-for-buffer #:prompt-for-file #:prompt-for-integer
   #:prompt-for-keyword #:prompt-for-expression #:prompt-for-string
   #:prompt-for-variable #:prompt-for-yes-or-no #:prompt-for-y-or-n
   #:prompt-for-key-event #:prompt-for-key
   #:logical-key-event-p
   #:logical-key-event-key-events
   #:define-logical-key-event #:*parse-type* #:current-variable-tables

   ;; files.lisp
   #:read-file #:write-file


   ;; font.lisp
   #:font-mark #:delete-font-mark #:delete-line-font-marks #:move-font-mark
   #:window-font

   ;; htext1.lisp
   #:line-length #:line-buffer #:line-string #:line-character #:mark #:mark-kind
   #:copy-mark #:delete-mark #:move-to-position #:region #:make-empty-region
   #:start-line-p #:end-line-p #:empty-line-p #:blank-line-p #:blank-before-p
   #:blank-after-p #:same-line-p #:mark< #:mark<= #:mark> #:mark>= #:mark= #:mark/=
   #:line< #:line<= #:line> #:line>= #:first-line-p #:last-line-p #:buffer-signature
   #:lines-related


   ;; htext2.lisp
   #:region-to-string #:string-to-region #:line-to-region
   #:previous-character #:next-character #:count-lines
   #:count-characters #:line-start #:line-end #:buffer-start
   #:buffer-end #:move-mark #:mark-before #:mark-after
   #:character-offset #:line-offset #:region-bounds
   #:set-region-bounds #:*print-region*

   ;; htext3.lisp
   #:insert-character #:insert-string #:insert-region #:ninsert-region

   ;; htext4.lisp
   #:delete-characters #:delete-region #:delete-and-save-region #:copy-region
   #:filter-region

   ;; interp.lisp
   #:bind-key #:delete-key-binding #:get-command #:map-bindings
   #:make-command #:command-name #:command-bindings #:last-command-type
   #:prefix-argument #:exit-hemlock #:*invoke-hook* #:key-translation

   ;; main.lisp
   #:*global-variable-names* #:*mode-names* #:*buffer-names*
   #:*character-attribute-names* #:*command-names* #:*buffer-list*
   #:*window-list* #:*last-key-event-typed* #:after-editor-initializations
   #:*background-image*

   ;; screen.lisp
   #:make-window #:delete-window #:next-window #:previous-window

   ;; search1.lisp
   #:search-pattern #:search-pattern-p #:find-pattern #:replace-pattern
   #:new-search-pattern

   ;; streams.lisp
   #:make-hemlock-output-stream
   #:hemlock-region-stream #:hemlock-region-stream-p
   #:hemlock-output-stream #:make-hemlock-region-stream
   #:hemlock-output-stream-p #:make-kbdmac-stream
   #:modify-kbdmac-stream

   ;; syntax.lisp
   #:character-attribute-name
   #:defattribute #:character-attribute-documentation #:character-attribute
   #:character-attribute-hooks #:character-attribute-p #:shadow-attribute
   #:unshadow-attribute #:find-attribute #:reverse-find-attribute

   ;; vars.lisp
   #:variable-value #:variable-hooks #:variable-documentation #:variable-name
   #:hemlock-bound-p #:defhvar #:delete-variable

   ;; window.lisp
   #:current-window #:window-buffer #:modeline-field-width
   #:modeline-field-function #:make-modeline-field #:update-modeline-fields
   #:update-modeline-field #:modeline-field-name #:modeline-field
   #:editor-finish-output #:*window-list*

   ;; start hemlock
   #:xomax
   #:main
   #:with-editor
   #:call-with-editor
   #:linedit
   #:formedit
   #:repl
   #:concat))

(defpackage :hemlock
  (:use :common-lisp :hemlock-interface :hi :iterate)
  (:import-from :hemlock-internals
                #:xomax
                :*xdk-display*
                :key-event-char)
  (:export #:xomax
           #:main
           #:with-editor
           #:call-with-editor
           #:start-slave
           #:*background-image*
           #:linedit
           #:formedit
           #:repl))

(defpackage :hemlock-user
    (:use :common-lisp :hemlock-interface))

(defpackage :hemlock.terminfo
  (:use :common-lisp)
  (:export #:*terminfo-directories*
           #:*terminfo*
           #:capability
           #:tparm
           #:tputs
           #:set-terminal))
