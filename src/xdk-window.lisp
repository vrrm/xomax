;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :hemlock-internals)

(defclass bitmap-device (device)
  ((display :initarg :display
            :initform nil
            :accessor bitmap-device-display
            :documentation "CLX display object.")))

;;; The lock field is no longer used.  If events could be handled
;;; while we were in the middle of something with the hunk, then this
;;; could be set for exclusion purposes.
(defclass bitmap-hunk (device-hunk)
  ((width
    :initarg :width
    :initform nil
    :accessor bitmap-hunk-width
    :documentation "Pixel width.")
   (char-height
    :initarg :char-height
    :initform nil
    :accessor bitmap-hunk-char-height
    :documentation "Height of text body in characters.")
   (char-width
    :initarg :char-width
    :initform nil
    :accessor bitmap-hunk-char-width
    :documentation "Width in characters.")
   (start
    :initarg :start
    :initform nil
    :accessor bitmap-hunk-start
    :documentation "Head of dis-line list (no dummy).")
   (end
    :initarg :end
    :initform nil
    :accessor bitmap-hunk-end
    :documentation "Exclusive end, i.e. nil if nil-terminated.")
   (modeline-dis-line
    :initarg :modeline-dis-line
    :initform nil
    :accessor bitmap-hunk-modeline-dis-line
    :documentation "Dis-line for modeline, or NIL if none.")
   (modeline-pos
    :initarg :modeline-pos
    :initform nil
    :accessor bitmap-hunk-modeline-pos
    :documentation "Position of modeline in pixels.")
   (lock
    :initarg :lock
    :initform t
    :accessor bitmap-hunk-lock
    :documentation "Something going on, set trashed if we're changed.")
   (trashed
    :initarg :trashed
    :initform nil
    :accessor bitmap-hunk-trashed
    :documentation "Something bad happened, recompute image.")
   (font-family
    :initarg :font-family
    :initform nil
    :accessor bitmap-hunk-font-family
    :documentation "Font-family used in this window.")
   (input-handler
    :initarg :input-handler
    :initform nil
    :accessor bitmap-hunk-input-handler
    :documentation "Gets hunk, char, x, y when char read.")
   (changed-handler
    :initarg :changed-handler
    :initform nil
    :accessor bitmap-hunk-changed-handler
    :documentation "Gets hunk when size changed.")
   (thumb-bar-p
    :initarg :thumb-bar-p
    :initform nil
    :accessor bitmap-hunk-thumb-bar-p
    :documentation "True if we draw a thumb bar in the top border.")
   (window-group
    :initarg :window-group
    :initform nil
    :accessor bitmap-hunk-window-group
    :documentation "The window-group to which this hunk belongs.")))

(defclass xdk-window (bitmap-hunk)
  ((xwindow
    :initarg :xwindow
    :initform nil
    :accessor bitmap-hunk-xwindow
    :documentation "X window for this hunk.")
   (type
    :initarg type
    :accessor type
    :documentation "one of: :root, :toplevel, :child, :dialog, :temp, :pixmap")
   (gcontext
    :initarg :gcontext
    :initform nil
    :accessor bitmap-hunk-gcontext
    :documentation "X gcontext for xwindow.")))

(defun bitmap-hunk-height (hunk)
  (device-hunk-height hunk))

(defun (setf bitmap-hunk-height) (value hunk)
  (setf (device-hunk-height hunk) value))

(defun bitmap-hunk-window (hunk)
  (device-hunk-window hunk))

(defun (setf bitmap-hunk-window) (value hunk)
  (setf (device-hunk-window hunk) value))

(defun bitmap-hunk-previous (hunk)
  (device-hunk-previous hunk))

(defun (setf bitmap-hunk-previous) (value hunk)
  (setf (device-hunk-previous hunk) value))

(defun bitmap-hunk-next (hunk)
  (device-hunk-next hunk))

(defun (setf bitmap-hunk-next) (value hunk)
  (setf (device-hunk-next hunk) value))

(defun bitmap-hunk-device (hunk)
  (device-hunk-device hunk))

(defun (setf bitmap-hunk-device) (value hunk)
  (setf (device-hunk-device hunk) value))

(defun bitmap-hunk-position (hunk)
  (device-hunk-position hunk))

(defun (setf bitmap-hunk-position) (value hunk)
  (setf (device-hunk-position hunk) value))

(defun make-xdk-window (&rest initargs)
  (apply #'make-instance 'xdk-window initargs))

(defgeneric hunk-put-string* (hunk x y font-family font string start end))
(defgeneric old-hunk-replace-line (hunk dl &optional position))
(defgeneric hunk-clear-lines (hunk start count))
(defgeneric hunk-copy-lines (hunk src dst count))
(defgeneric hunk-replace-modeline (hunk))



;; struct _GdkWindow
;; {
;;   GdkVisual     *visual;
;;   GdkColormap   *colormap;

;;   gint16   x;
;;   gint16   y;
;;   guint16  width;
;;   guint16  height;
;;   gint16   depth;

;;   GdkWindow  *parent;
;;   GdkWindow  *children;
;;   GdkWindow  *next_sibling;
;;   GdkWindow  *prev_sibling;

;;   gpointer user_data;

;;   GdkWindow window;
;;   Window xwindow;
;;   Display *xdisplay;
;;   unsigned int destroyed : 1;
;; };



;; };

;;; The default hook-function for random typeout.  Nothing very fancy
;;; for now.  If not given a window, makes one on top of the initial
;;; Hemlock window using specials set in INIT-BITMAP-SCREEN-MANAGER.
;;; If given a window, we will change the height subject to the
;;; constraint that the bottom won't be off the screen.  Any resulting
;;; window has input and boundary crossing events selected, a hemlock
;;; cursor defined, and is mapped.
(defun custom-create-xdk-window (device window height)
  (declare (fixnum height))
  (let* ((display (bitmap-device-display device))
         (root (xlib:screen-root (screen *xdk-display*)))
         (full-height (xlib:drawable-height root))
         (actual-height (if window
                            (multiple-value-bind (x y) (window-root-xy window)
                              (declare (ignore x) (fixnum y))
                              (min (- full-height y xwindow-border-width*2)
                                   height))
                            (min (- full-height *random-typeout-start-y*
                                    xwindow-border-width*2)
                                 height)))
         (win (cond (window
                     (setf (xlib:drawable-height window) actual-height)
                     window)
                    (t
                     (let ((win (xlib:create-window
                                 :parent root
                                 :x *random-typeout-start-x*
                                 :y *random-typeout-start-y*
                                 :width *random-typeout-start-width*
                                 :height actual-height
                                 :background (default-background-pixel *xdk-display*)
                                 :border-width xwindow-border-width
                                 :border (default-border-pixmap *xdk-display*)
                                 :event-mask random-typeout-xevents-mask
                                 :override-redirect :on :class :input-output
                                 :cursor *hemlock-cursor*)))
                       (xlib:set-wm-properties
                        win :name "Pop-up Display" :icon-name "Pop-up Display"
                        :resource-name "Hemlock"
                        :x *random-typeout-start-x*
                        :y *random-typeout-start-y*
                        :width *random-typeout-start-width*
                        :height actual-height
                        :user-specified-position-p t :user-specified-size-p t
                        ;; Tell OpenLook pseudo-X11 server we want input.
                        :input :on)
                       win))))
         (gcontext (if (not window) (default-gcontext win))))
    (values win gcontext)))

;;; Make an X window with parent.  x, y, w, and h are possibly nil, so
;;; we supply zero in this case.  This would be used for prompting the
;;; user.  Some standard properties are set to keep window managers in
;;; line.  We name all windows because awm and twm window managers
;;; refuse to honor menu clicks over windows without names.  Min-width
;;; and min-height are optional and only used for prompting the user
;;; for a window.
(defun create-window-with-properties (parent x y w h font-width font-height
                                      icon-name
                                      &optional min-width min-height
                                        window-group-p)
  (let* ((win (xlib:create-window
               :parent parent :x (or x 0) :y (or y 0)
               :width (or w 0) :height (or h 0)
               :background (if window-group-p
                               (default-margin-pixel *xdk-display*) ; :none
                               (default-background-pixel *xdk-display*))
               :border-width (if window-group-p xwindow-border-width 0)
               :border (if window-group-p (default-border-pixmap *xdk-display*) nil)
               :class :input-output)))
    (xlib:set-wm-properties
     win :name (new-hemlock-window-name) :icon-name icon-name
     :resource-name "Hemlock"
     :x x :y y :width w :height h
     :user-specified-position-p t :user-specified-size-p t
     :width-inc font-width :height-inc font-height
     :min-width min-width :min-height min-height
     ;; Tell OpenLook pseudo-X11 server we want input.
     :input :on)
    win))
