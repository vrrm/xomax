;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;
(in-package :hi)


;;;; PREPL/background buffer integration

(declaim (special hi::*in-hemlock-slave-p*
                  hemlock::*master-machine-and-port*
                  hemlock::*original-terminal-io*))

(defun need-to-redirect-debugger-io (stream)
  (eq stream hemlock::*original-terminal-io*))
