;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :hemlock-internals)

(defmethod make-event-loop ((backend (eql :iolib)))
  (make-instance 'iolib:event-base
                 :mux 'iolib.multiplex:select-multiplexer))

(defvar *xdk-display* nil)
(defvar *connection-backend* :iolib)
(defvar *main-event-base* (make-event-loop :iolib))


;;; Display is the top level xdk object.  To use any other library
;;; feature the library must first be initialized.  Exiting leave the
;;; system in a state that allows it to be intialized again without
;;; stopping Lisp.  Not useful for most application, but helpful when
;;; debugging and for incremental development.

(defclass xdk-display ()
  ((display :initarg :display :accessor display)
   (screen :initarg :screen :accessor screen)
   (root-window :initarg :root-window :accessor root-window)
   (handler :initarg :handler :accessor handler)
   (event-base :initarg :event-base :accessor event-base)
   (editor-input :initarg :editor-input :accessor editor-input)
   (default-background-pixel
       :initarg :default-background-pixel :accessor default-background-pixel)
   (default-foreground-pixel
       :initarg :default-foreground-pixel :accessor default-foreground-pixel)
   (default-margin-pixel
       :initarg :default-margin-pixel :accessor default-margin-pixel)
   (cursor-background-color
    :initarg :cursor-background-color :accessor cursor-background-color)
   (cursor-foreground-color
    :initarg :cursor-foreground-color :accessor cursor-foreground-color)
   (foreground-background-xor
    :initarg :foreground-background-xor :accessor foreground-background-xor)
   (highlight-border-pixmap
    :initarg :highlight-border-pixmap :accessor highlight-border-pixmap)
   (default-border-pixmap
       :initarg :default-border-pixmap :accessor default-border-pixmap)
   ))

(defun make-pixel (screen color)
  (rgb-pixel (xlib:screen-root-visual-info screen) color))

(defun create-xdk-display ()
  (let* ((display (xlib::open-display ""))
         (screen (xlib:display-default-screen display))
         (root-window (xlib:screen-root screen))
         (default-background-pixel (make-pixel screen #xb3b3df))
         (default-foreground-pixel (make-pixel screen #x0000aa))
         (bm
          (make-instance
           'xdk-display
           :display display
           :event-base (make-event-loop :iolib)
           :screen screen
           :root-window root-window
           :handler #'object-set-event-handler
           :editor-input (make-windowed-editor-input)
           :default-background-pixel default-background-pixel
           :default-foreground-pixel default-foreground-pixel
           :default-margin-pixel default-background-pixel
           :cursor-background-color (make-white-color)
           :cursor-foreground-color (make-black-color)
           :foreground-background-xor (logxor
                                       default-foreground-pixel
                                       default-background-pixel)
           :highlight-border-pixmap default-foreground-pixel
           :default-border-pixmap (get-hemlock-grey-pixmap
                                   screen
                                   default-foreground-pixel
                                   default-background-pixel)
           )))
    bm))

;;; Load display cursor
(defun get-hemlock-cursor ()
  (when *hemlock-cursor* (xlib:free-cursor *hemlock-cursor*))
  (let* ((cursor-file (truename (variable-value 'hemlock::cursor-bitmap-file)))
         (mask-file (probe-file (make-pathname :type "mask"
                                               :defaults cursor-file)))
         (root (xlib:screen-root (screen *xdk-display*)))
         (mask-pixmap (if mask-file (get-cursor-pixmap root mask-file))))
    (multiple-value-bind (cursor-pixmap cursor-x-hot cursor-y-hot)
        (get-cursor-pixmap root cursor-file)
      (setf *hemlock-cursor*
            (xlib:create-cursor :source cursor-pixmap :mask mask-pixmap
                                :x cursor-x-hot :y cursor-y-hot
                                :foreground (cursor-foreground-color *xdk-display*)
                                :background (cursor-background-color *xdk-display*)))
      (xlib:free-pixmap cursor-pixmap)
      (when mask-pixmap (xlib:free-pixmap mask-pixmap)))))


  (defmethod xdk-init ()
    (let ((xdk-display (create-xdk-display)))
      (with-slots (display handler) xdk-display
        (setup-font-family display)
        (enable-clx-event-handling xdk-display)
        ;; Make an initial window, and set up redisplay's internal
        ;; data structures.
        (setf *xdk-display* xdk-display))
      ))

  (defmethod xdk-exit ()
    (with-slots (display handler event-base) *xdk-display*
      (iolib:remove-fd-handlers event-base (stream-fd (xlib::display-input-stream display)))
      (xlib:close-display display)
      (setf *xdk-display* nil)))

  (defmethod dispatch-events-with-backend ((backend (eql :iolib)))
    (handler-case
        (iolib:event-dispatch (event-base *xdk-display*) :one-shot t :min-step 0)
      ((or isys:etimedout isys:ewouldblock) ())))

  (defmethod dispatch-events-no-hang-with-backend ((backend (eql :iolib)))
    (handler-case
        (iolib:event-dispatch (event-base *xdk-display*)
          :one-shot t
          :timeout 0
          :min-step 0)
      ((or isys:etimedout isys:ewouldblock) ())))


  (defmethod invoke-later ((backend (eql :iolib)) fun)
    (iolib.multiplex:add-timer (event-base *xdk-display*) fun 0 :one-shot t))


  (defun set-iolib-handlers (connection)
    (let ((fd (connection-read-fd connection)))
      (iolib:set-io-handler
       (event-base *xdk-display*)
       fd
       :read
       (lambda (.fd event error)
         (declare (ignore event))
         (when (or (eq error :error)
                   (eq (process-incoming-data connection) :eof))
           (iolib:remove-fd-handlers (event-base *xdk-display*) fd :read t))))))


  (defmethod connection-write (data (connection iolib-connection))
    (let ((bytes (filter-connection-output connection data))
          (fd (connection-write-fd connection))
          (need-handler (null (connection-write-buffers connection)))
          handler)
      (check-type bytes (simple-array (unsigned-byte 8) (*)))
      (setf (connection-write-buffers connection)
            (nconc (connection-write-buffers connection)
                   (list bytes)))
      (when need-handler
        (setf handler
              (iolib:set-io-handler
               (event-base *xdk-display*)
               fd
               :write
               (lambda (.fd event error)
                 (declare (ignore event))
                 (when (eq error :error) (error "error with ~A" .fd))
                 ;; fixme: with-pointer-to-vector-data isn't portable
                 (let ((bytes (pop (connection-write-buffers connection))))
                   (check-type bytes (simple-array (unsigned-byte 8) (*)))
                   (cffi-sys:with-pointer-to-vector-data (ptr bytes)
                     (let ((n-bytes-written
                            (isys:write fd ptr (length bytes))))
                       (unless (eql n-bytes-written (length bytes))
                         (push (subseq bytes n-bytes-written)
                               (connection-write-buffers connection)))))
                   (setf (iolib.multiplex::fd-handler-one-shot-p handler)
                         (null (connection-write-buffers connection))))))))))

  (defun set-iolib-server-handlers (instance)
    (iolib:set-io-handler
     (event-base *xdk-display*)
     (connection-fd instance)
     :read
     (lambda (.fd event error)
       (declare (ignore event))
       (when (eq error :error) (error "error with ~A" .fd))
       (process-incoming-connection instance))))

;;; ENABLE-CLX-EVENT-HANDLING associates the display with the handler in
;;; *display-event-handlers*.  It also uses SYSTEM:ADD-FD-HANDLER to have
;;; SYSTEM:SERVE-EVENT call CALL-DISPLAY-EVENT-HANDLER whenever anything shows
;;; up from the display. Since CALL-DISPLAY-EVENT-HANDLER is called on a
;;; file descriptor, the file descriptor is also mapped to the display in
;;; *clx-fds-to-displays*, so the user's handler can be called on the display.
;;;
  (defun enable-clx-event-handling (xdk-display)
    "After calling this, when SYSTEM:SERVE-EVENT notices input on display's
   connection to the X11 server, handler is called on the display.  Handler
   is invoked in a dynamic context with an error handler bound that will
   flush all events from the display and return.  By returning, it declines
   to handle the error, but it will have cleared all events; thus, entering
   the debugger will not result in infinite errors due to streams that wait
   via SYSTEM:SERVE-EVENT for input.  Calling this repeatedly on the same
   display establishes handler as a new handler, replacing any previous one
   for display."
    (with-slots (display handler) xdk-display
      (let ((change-handler (assoc display *display-event-handlers*)))
        (if change-handler
            (setf (cdr change-handler) handler)
            (let ((fd (hi::stream-fd (xlib::display-input-stream display))))
              (iolib:set-io-handler
               (event-base xdk-display) fd :read #'call-display-event-handler)
              (setf (gethash fd *clx-fds-to-displays*) display)
              (push (cons display handler) *display-event-handlers*))))))

;;; CALL-DISPLAY-EVENT-HANDLER maps the file descriptor to its display and maps
;;; the display to its handler.  If we can't find the display, we remove the
;;; file descriptor using SYSTEM:INVALIDATE-DESCRIPTOR and try to remove the
;;; display from *display-event-handlers*.  This is necessary to try to keep
;;; SYSTEM:SERVE-EVENT from repeatedly trying to handle the same event over and
;;; over.  This is possible since many CMU Common Lisp streams loop over
;;; SYSTEM:SERVE-EVENT, so when the debugger is entered, infinite errors are
;;; possible.
;;;
  (defun call-display-event-handler (file-descriptor event error)
    (if (eq error :error)
        (iolib:remove-fd-handlers (event-base *xdk-display*) file-descriptor :read t)
        (let ((display (gethash file-descriptor *clx-fds-to-displays*)))
          (unless display
            (iolib.multiplex:remove-fd-handlers (event-base *xdk-display*) file-descriptor)
            (setf *display-event-handlers*
                  (delete file-descriptor *display-event-handlers*
                          :key #'(lambda (d/h)
                                   (hi::stream-fd
                                    (xlib::display-input-stream
                                     (car d/h))))))
            (error "File descriptor ~S not associated with any CLX display.~%~
                  It has been removed from system:serve-event's knowledge."
                   file-descriptor))
          (let ((handler (cdr (assoc display *display-event-handlers*))))
            (unless handler
              (flush-display-events display)
              (error "Display ~S not associated with any event handler."
                     display))
            (handler-bind ((error #'(lambda (condx)
                                      (declare (ignore condx))
                                      (flush-display-events display))))
              (funcall handler display))))))



  (defun disable-clx-event-handling (display)
    "Undoes the effect of EXT:ENABLE-CLX-EVENT-HANDLING."
    (setf *display-event-handlers*
          (delete display *display-event-handlers* :key #'car))
    (let ((fd (hi::stream-fd (xlib::display-input-stream display))))
      (remhash fd *clx-fds-to-displays*)
      ;; (iolib:remove-fd-handlers (event-base *xdk-display*) fd :read t)
      ))
