;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :hemlock-internals)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar group-interesting-xevents
    '(:structure-notify)))

(defvar group-interesting-xevents-mask
  (apply #'xlib:make-event-mask group-interesting-xevents))

;; (eval-when (:compile-toplevel :load-toplevel :execute)
;;   (defvar child-interesting-xevents
;;     '(:key-press
;;       :key-release
;;       :button-press
;;       :button-release
;;       :structure-notify :exposure
;;       :enter-window :leave-window)))

(defvar child-interesting-xevents-mask
  (apply #'xlib:make-event-mask '(:key-press
      :key-release
      :button-press
      :button-release
      :structure-notify :exposure
      :enter-window :leave-window)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar random-typeout-xevents
    '(:key-press
      :key-release
      :button-press
      :button-release
      :enter-window :leave-window
                 :exposure)))

(defvar random-typeout-xevents-mask
  (apply #'xlib:make-event-mask random-typeout-xevents))

(declaim (special hemlock::*open-paren-highlight-font*
                  hemlock::*active-region-highlight-font*))

(defparameter lisp-fonts-pathnames '("fonts/"))

;;; SETUP-FONT-FAMILY sets *default-font-family*, opening the three font names
;;; passed in.  The font family structure is filled in from the first argument.
;;; Actually, this ignores default-highlight-font and default-open-paren-font
;;; in lieu of "Active Region Highlighting Font" and "Open Paren Highlighting
;;; Font" when these are defined.
;;;
(defun setup-font-family (display)
  (let* ((font-family (make-font-family :map (make-array font-map-size
                                                         :initial-element 0)
                                        :cursor-x-offset 0
                                        :cursor-y-offset 0))
         (font-family-map (font-family-map font-family)))
    (declare (simple-vector font-family-map))
    (setf *default-font-family* font-family)
    (let ((font (xlib:open-font display (variable-value 'hemlock::default-font))))
      (unless font
        (error "Cannot open font -- ~S" (variable-value 'hemlock::default-font)))
      (fill font-family-map font)
      (let ((width (xlib:max-char-width font)))
        (setf (font-family-width font-family) width)
        (setf (font-family-cursor-width font-family) width))
      (let* ((baseline (xlib:font-ascent font))
             (height (+ baseline (xlib:font-descent font))))
        (setf (font-family-height font-family) height)
        (setf (font-family-cursor-height font-family) height)
        (setf (font-family-baseline font-family) baseline)))
    (setup-one-font display
                    (variable-value 'hemlock::open-paren-highlighting-font)
                    font-family-map
                    hemlock::*open-paren-highlight-font*)
    (setup-one-font display
                    (variable-value 'hemlock::active-region-highlighting-font)
                    font-family-map
                    hemlock::*active-region-highlight-font*)
    ;; GB
    (setup-one-font display
                    "-*-lucidatypewriter-medium-r-*-*-*-120-*-*-*-*-iso8859-1"
                    font-family-map
                    7)))

;;; SETUP-ONE-FONT tries to open font-name for display, storing the result in
;;; font-family-map at index.  XLIB:OPEN-FONT will return font stuff regardless
;;; if the request is valid or not, so we finish the output to get synch'ed
;;; with the server which will cause any errors to get signaled.  At this
;;; level, we want to deal with this error here returning nil if the font
;;; couldn't be opened.
;;;
(defun setup-one-font (display font-name font-family-map index)
  (handler-case (let ((font (xlib:open-font display (namestring font-name))))
                  (xlib:display-finish-output display)
                  (setf (svref font-family-map index) font))
    (xlib:name-error ()
     (warn "Cannot open font -- ~S" font-name)
     nil)))


(defhvar "Raise Echo Area When Modified"
  "When set, Hemlock raises the echo area window when output appears there."
  :value nil)

;;; RAISE-ECHO-AREA-WHEN-MODIFIED -- Internal.
;;;
;;; INIT-BITMAP-SCREEN-MANAGER in bit-screen.lisp adds this hook when
;;; initializing the bitmap screen manager.
;;;
(defun raise-echo-area-when-modified (buffer modified)
  (when (and (value hemlock::raise-echo-area-when-modified)
             (eq buffer *echo-area-buffer*)
             modified)
    (let* ((hunk (window-hunk *echo-area-window*))
           (win (window-group-xparent (bitmap-hunk-window-group hunk))))
      (xlib:map-window win)
      (setf (xlib:window-priority win) :above)
      (xlib:display-force-output
       (bitmap-device-display (device-hunk-device hunk))))))

;; these came from RE-INITIALIZE-KEY-EVENTS:
(define-clx-modifier (xlib:make-state-mask :shift) "Shift")
(define-clx-modifier (xlib:make-state-mask :mod-1) "Meta")
(define-clx-modifier (xlib:make-state-mask :control) "Control")
(define-clx-modifier (xlib:make-state-mask :lock) "Lock")

(defun get-hemlock-grey-pixmap (screen default-foreground-pixel default-background-pixel)
  (let* ((depth (xlib:screen-root-depth screen))
         (root (xlib:screen-root screen))
         (height (length hemlock-grey-bitmap-data))
         (width (length (car hemlock-grey-bitmap-data)))
         (image (apply #'xlib:bitmap-image hemlock-grey-bitmap-data))
         (pixmap (xlib:create-pixmap :width width :height height
                                     :depth depth :drawable root))
         (gc (xlib:create-gcontext :drawable pixmap
                                   :function boole-1
                                   :foreground default-foreground-pixel
                                   :background default-background-pixel)))
    (xlib:put-image pixmap gc image
                    :x 0 :y 0 :width width :height height :bitmap-p t)
    (xlib:free-gcontext gc)
    pixmap))


;;; translate-key-event -- Public.
(defun translate-key-event (display scan-code bits)
  "Translates the X scan-code and X bits to a key-event.  First this maps
   scan-code to an X keysym using XLIB:KEYCODE->KEYSYM looking at bits and
   supplying index as 1 if the X shift bit is on, 0 otherwise.

   If the resulting keysym is undefined, and it is not a modifier keysym, then
   this signals an error.  If the keysym is a modifier key, then this returns
   nil.

   If the following conditions are satisfied
      the keysym is defined
      the X shift bit is off
      the X lock bit is on
      the X keysym represents a lowercase letter
   then this maps the scan-code again supplying index as 1 this time, treating
   the X lock bit as a caps-lock bit.  If this results in an undefined keysym,
   this signals an error.  Otherwise, this makes a key-event with the keysym
   and bits formed by mapping the X bits to key-event bits.

   If any state bit is set that has no suitable modifier translation, it is
   passed to XLIB:DEFAULT-KEYSYM-INDEX in order to handle Mode_Switch keys
   appropriately.

   Otherwise, this makes a key-event with the keysym and bits formed by mapping
   the X bits to key-event bits."
  (let ((new-bits 0)
        shiftp lockp)
    (dolist (map *modifier-translations*)
      (unless (zerop (logand (car map) bits))
        ;; ignore the bits of the mapping for the determination of a key index
        (setq bits (logxor bits (car map)))
        (cond
         ((string-equal (cdr map) "Shift")
          (setf shiftp t))
         ((string-equal (cdr map) "Lock")
          (setf lockp t))
         (t (setf new-bits
                  (logior new-bits (key-event-modifier-mask (cdr map))))))))
    ;; here pass any remaining modifier bits to clx
    (let* ((index  (and (not (zerop bits))
                        (xlib:default-keysym-index display scan-code bits)))
           (keysym (xlib:keycode->keysym display scan-code (or index (if shiftp 1 0)))))
      (cond ((null (keysym-names keysym))
             nil)
            ((and (not shiftp) lockp (<= 97 keysym 122)) ; small-alpha-char-p
             (let ((keysym (xlib:keycode->keysym display scan-code 1)))
               (if (keysym-names keysym)
                   (make-key-event keysym new-bits)
                   nil)))
            (t
             (make-key-event keysym new-bits))))))


;;;; Object set event handling.

;;; This is bound by OBJECT-SET-EVENT-HANDLER, so DISPATCH-EVENT can clear
;;; events on the display before signalling any errors.  This is necessary
;;; since reading on certain CMU Common Lisp streams involves SERVER, and
;;; getting an error while trying to handle an event causes repeated attempts
;;; to handle the same event.
;;;
(defvar *process-clx-event-display* nil)

(defvar *object-set-event-handler-print* nil)

(defun get-hash-as-assoc-list (hash)
  (let ((assoc-list '()))
    (maphash (lambda (key value)
               (setf assoc-list (cons (pairlis '(:value :key) (list value key)) assoc-list)))
             hash)
    assoc-list))



(defmacro dispatch (event-key &rest args)
  `(multiple-value-bind (object object-set)
       (lisp--map-xwindow event-window)
     (unless object
       (cond ((not (typep event-window 'xlib:window))
              ;;(xlib:discard-current-event display)
              (warn "Discarding ~S event on non-window ~S."
                    ,event-key event-window)
              (return-from object-set-event-handler nil)
              )
             (t
              (flush-display-events display)
              (error "~S not a known X window.~%~
                                   Received event ~S."
                     event-window ,event-key))))
     (handler-bind ((error #'(lambda (condx)
                               (declare (ignore condx))
                               (flush-display-events display))))
       (when *object-set-event-handler-print*
         (print ,event-key) (force-output))
       (funcall (gethash ,event-key
                         (object-set-table object-set)
                         (object-set-default-handler
                          object-set))
                object ,event-key
                ,@args))
     (setf result t)))

(defun object-set-event-handler (display &optional (timeout 0))
  "This display event handler uses object sets to map event windows cross
   event types to handlers.  It uses XLIB:EVENT-CASE to bind all the slots
   of each event, calling the handlers on all these values in addition to
   the event key and send-event-p.  Describe EXT:SERVE-MUMBLE, where mumble
   is an event keyword name for the exact order of arguments.
   :mapping-notify and :keymap-notify events are ignored since they do not
   occur on any particular window.  After calling a handler, each branch
   returns t to discard the event.  While the handler is executing, all
   errors go through a handler that flushes all the display's events and
   returns.  This prevents infinite errors since the debug and terminal
   streams loop over SYSTEM:SERVE-EVENT.  This function returns t if there
   were some event to handle, nil otherwise.  It returns immediately if
   there is no event to handle."
  (let ((*process-clx-event-display* display)
        (result nil))
    (xlib:event-case (display :timeout timeout)
      ((:key-press :key-release :button-press :button-release)
       (event-key event-window root child same-screen-p
                  x y root-x root-y state time code send-event-p)
       (dispatch event-key event-window root child same-screen-p
                 x y root-x root-y state time code send-event-p))
      (:motion-notify (event-window root child same-screen-p
                                    x y root-x root-y state time hint-p send-event-p)
                      (dispatch :motion-notify event-window root child same-screen-p
                                x y root-x root-y state time hint-p send-event-p))
      (:enter-notify (event-window root child same-screen-p
                                   x y root-x root-y state time mode kind send-event-p)
                     (dispatch :enter-notify event-window root child same-screen-p
                               x y root-x root-y state time mode kind send-event-p))
      (:leave-notify (event-window root child same-screen-p
                                   x y root-x root-y state time mode kind send-event-p)
                     (dispatch :leave-notify event-window root child same-screen-p
                               x y root-x root-y state time mode kind send-event-p))
      (:exposure (event-window x y width height count send-event-p)
                 (dispatch :exposure event-window x y width height count send-event-p))
      (:graphics-exposure (event-window x y width height count major minor
                                        send-event-p)
                          (dispatch :graphics-exposure event-window x y width height
                                    count major minor send-event-p))
      (:no-exposure (event-window major minor send-event-p)
                    (dispatch :no-exposure event-window major minor send-event-p))
      (:focus-in (event-window mode kind send-event-p)
                 (dispatch :focus-in event-window mode kind send-event-p))
      (:focus-out (event-window mode kind send-event-p)
                  (dispatch :focus-out event-window mode kind send-event-p))
      (:keymap-notify ()
                      (warn "Ignoring keymap notify event.")
                      (when *object-set-event-handler-print*
                        (print :keymap-notify) (force-output))
                      (setf result t))
      (:visibility-notify (event-window state send-event-p)
                          (dispatch :visibility-notify event-window state send-event-p))
      (:create-notify (event-window window x y width height border-width
                                    override-redirect-p send-event-p)
                      (dispatch :create-notify event-window window x y width height
                                border-width override-redirect-p send-event-p))
      (:destroy-notify (event-window window send-event-p)
                       (dispatch :destroy-notify event-window window send-event-p))
      (:unmap-notify (event-window window configure-p send-event-p)
                     (dispatch :unmap-notify event-window window configure-p send-event-p))
      (:map-notify (event-window window override-redirect-p send-event-p)
                   (dispatch :map-notify event-window window override-redirect-p
                             send-event-p))
      (:map-request (event-window window send-event-p)
                    (dispatch :map-request event-window window send-event-p))
      (:reparent-notify (event-window window parent x y override-redirect-p
                                      send-event-p)
                        (dispatch :reparent-notify event-window window parent x y
                                  override-redirect-p send-event-p))
      (:configure-notify (event-window window x y width height border-width
                                       above-sibling override-redirect-p send-event-p)
                         (dispatch :configure-notify event-window window x y width height
                                   border-width above-sibling override-redirect-p
                                   send-event-p))
      (:gravity-notify (event-window window x y send-event-p)
                       (dispatch :gravity-notify event-window window x y send-event-p))
      (:resize-request (event-window width height send-event-p)
                       (dispatch :resize-request event-window width height send-event-p))
      (:configure-request (event-window window x y width height border-width
                                        stack-mode above-sibling value-mask send-event-p)
                          (dispatch :configure-request event-window window x y width height
                                    border-width stack-mode above-sibling value-mask
                                    send-event-p))
      (:circulate-notify (event-window window place send-event-p)
                         (dispatch :circulate-notify event-window window place send-event-p))
      (:circulate-request (event-window window place send-event-p)
                          (dispatch :circulate-request event-window window place send-event-p))
      (:property-notify (event-window atom state time send-event-p)
                        (dispatch :property-notify event-window atom state time send-event-p))
      (:selection-clear (event-window selection time send-event-p)
                        (dispatch :selection-notify event-window selection time send-event-p))
      (:selection-request (event-window requestor selection target property
                                        time send-event-p)
                          (dispatch :selection-request event-window requestor selection target
                                    property time send-event-p))
      (:selection-notify (event-window selection target property time
                                       send-event-p)
                         (dispatch :selection-notify event-window selection target property time
                                   send-event-p))
      (:colormap-notify (event-window colormap new-p installed-p send-event-p)
                        (dispatch :colormap-notify event-window colormap new-p installed-p
                                  send-event-p))
      (:mapping-notify (request)
                       (warn "Ignoring mapping notify event -- ~S." request)
                       (when *object-set-event-handler-print*
                         (print :mapping-notify) (force-output))
                       (setf result t))
      (:client-message (event-window format data send-event-p)
                       (dispatch :client-message event-window format data send-event-p)))
    result))

(defun default-clx-event-handler (object event-key event-window &rest ignore)
  (declare (ignore ignore))
  (flush-display-events *process-clx-event-display*)
  (break)
  (error "No handler for event type ~S on ~S in ~S."
         event-key object (lisp--map-xwindow event-window)))

(defun flush-display-events (display)
  "Dumps all the events in display's event queue including the current one
   in case this is called from within XLIB:EVENT-CASE, etc."
  (xlib:discard-current-event display)
  (xlib:event-case (display :discard-p t :timeout 0)
    (t () nil)))

(defvar *display-event-handlers* nil)


(defvar *clx-fds-to-displays* (make-hash-table))
